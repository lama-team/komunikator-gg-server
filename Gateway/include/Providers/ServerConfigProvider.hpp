#ifndef SERVER_CONFIG_PROVIDER_H
#define SERVER_CONFIG_PROVIDER_H
#include <iostream>
#include <sstream>  
#include <string>

class ServerConfigProvider {
public:
    virtual unsigned short GetPort() const {
        int port = 1024;
        auto portEnv = std::getenv("PORT");
        if(portEnv != NULL){
            std::stringstream portValue;
            portValue << portEnv;
            portValue >> port;
        }
        return port;
    }
    virtual std::string GetOracleConnectionString() const {
        return "service=//" + GetOracleUri() +"/xe user=system password=oracle";
    }
    virtual std::string GetOracleUri() const {
        std::string uri = "localhost:1521/xe";
        auto ORACLE_URI = std::getenv("ORACLE_URI");
        if(ORACLE_URI != NULL){
            std::stringstream oracleUriValue;
            oracleUriValue << ORACLE_URI;
            oracleUriValue >> uri;
        }
        return uri;
    }
    virtual unsigned int GetReceiveBufferSize() const {
        return 4096;
    }
    virtual unsigned int GetSendBufferSize() const {
        return 4096;
    }
};
#endif //SERVER_CONFIG_PROVIDER_H