#ifndef LEGACY_SERVER_H
#define LEGACY_SERVER_H
#include <Providers/ServerConfigProvider.hpp>
#include <Model/Client.hpp>
#include <stdexcept>
#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <sstream> 

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/signals2.hpp>

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>

class Server{
public:
    Server(const ServerConfigProvider &provider) : provider(provider) {
        receiveBuf = std::vector<char>(provider.GetReceiveBufferSize());
    }
    void Run();
    // (fd, uuid, message)
    boost::signals2::signal<void (int, boost::uuids::uuid, std::string, std::string, std::string, std::string)> receivedSignal;
    void Send(int fd, boost::uuids::uuid uuid, std::string message);
private:
    void CreateSocket();
    void BindAndListen();
    void AcceptClient();
    void ReadFromClient(int i);
    void ResetMasks();
    const ServerConfigProvider &provider;
    std::vector<char> receiveBuf;
    struct timeval timeout;
    int fd;
    int on;
    struct sockaddr_in addr;

    int selectCount;
    int fdmax;
    fd_set rmask, wmask, rmask_new, wmask_new;
    std::map<int, Client> clients;
    boost::uuids::random_generator gen;
};

#endif //LEGACY_SERVER_H