#ifndef CLIENT_H
#define CLIENT_H
#include<string>
#include <boost/uuid/uuid.hpp>
#include <sys/socket.h>
#include <netinet/in.h>
#include <vector>
struct Client {
    struct sockaddr_in client_addr;
    socklen_t client_addr_length;
    int fd;
    std::vector<char> receiveBuf;
    boost::uuids::uuid uuid;
};
#endif //CLIENT_H