#ifndef HANDLER_H
#define HANDLER_H
#include <string>
#include <boost/uuid/uuid.hpp>
#include <sys/socket.h>
#include <netinet/in.h>
#include <vector>
#include <Client/ClientGateway.hpp>
#include <memory>
struct Handler {
    std::shared_ptr<ClientGateway> handler;
    bool requiresAuthentication;
};
#endif //HANDLER_H