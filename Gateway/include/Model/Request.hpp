#ifndef REQUEST_H
#define REQUEST_H
#include <boost/uuid/uuid.hpp>
#include <optional>
#include <string>

struct Request {
    int fd;
    boost::uuids::uuid uuid;
    boost::uuids::uuid correlationId;
    std::optional<std::string> login;
};
#endif //REQUEST_H