#ifndef USER_H
#define USER_H
#include <Model/BaseModel.hpp>
    
struct User{
    std::string Id;
    std::string HashedPassword;
    std::string Salt;
    std::string FirstName;
    std::string LastName;
    unsigned int Age;
    std::string Gender;
    std::string City;
    std::string Country;
};

#endif //USER_H