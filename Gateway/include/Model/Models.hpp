#ifndef USER_AVATAR_H
#define USER_AVATAR_H
#include <Model/BaseModel.hpp>

struct UserAvatar {
    std::string UserId;
    std::string Avatar;
};

struct Contact {
  std::string UserId;
  std::string Username;
  std::string Status;
  std::string Description;
  std::string Avatar;
  std::string FirstName;
  std::string LastName;
  std::string Country;
  std::string City;
  unsigned int Age;
  std::string Gender;
};

struct ContactsQuery {
  std::string Username;
  std::string FirstName;
  std::string LastName;
  std::string Country;
  std::string City;
  unsigned int MinAge;
  unsigned int MaxAge;
  std::string Gender;
};

struct ContactRequestModel{
    std::string SourceUserId;
    std::string TargetUserId;
    bool Accepted;
};

struct SettingModel{
    std::string Name;
    std::string Value;
};

struct SendMessageModel {
    std::string SourceUserId;
    std::string TargetUserId;
    std::string MessageId;
    std::string Content;
};

struct UserModel {
    std::string UserId;
    std::string Username;
    std::string Status;
    std::string Description;
    std::string Avatar;
    std::string FirstName;
    std::string LastName;
    std::string Country;
    std::string City;
    unsigned int Age;
    std::string Gender;
};
struct ContactRequest {
    std::string SourceUserId;
    std::string TargetUserId;
};

struct Descriptions{
    std::string UserId;
    std::tm Changed;
    std::string Description;
};

struct Message {
    std::string MessageId;
    std::string Message;
    std::tm Created;
    std::string SourceUserId;
    std::string TargetUserId;
};

struct ContactWithMessages {
  Contact ContactObj;
  std::vector<Message> Messages;
};

struct Setting {
    std::string Name;
    std::string Value;
};

struct UserState{
    UserModel User;
    std::vector<ContactRequest> ContactRequests;
    std::vector<ContactWithMessages> Contacts;
};

struct ContactRequestConfirmModel{
    std::string TargetUserId;
    std::string SourceUserId;
    bool Confirm;
};
#endif