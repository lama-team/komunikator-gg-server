#ifndef SEND_TO_CLIENT_H
#define SEND_TO_CLIENT_H
#include <string>
#include <boost/uuid/uuid.hpp>
class SendToClient {
public:
    virtual void Send(std::string message) const = 0;
};
#endif //SEND_TO_CLIENT_H