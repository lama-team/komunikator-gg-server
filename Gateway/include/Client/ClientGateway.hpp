#ifndef CLIENT_GATEWAY_H
#define CLIENT_GATEWAY_H
#include <Handlers/BaseHandler.hpp>
#include <Model/Request.hpp>
#include <boost/signals2.hpp>
#include <stdexcept>
#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <sstream> 
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <Server.hpp>
#include <messages.pb.h>
#include <Utilities/Helpers.hpp>
#include <Utilities/Validators.hpp>


class ClientGateway : BaseHandler{
public:
    ClientGateway(Server &server) : server(server){};
    virtual ~ClientGateway(){};
    virtual void Receive(std::string message) = 0;
    void Send(std::string message) const;
    Request requestContext;
    void SendSuccess(std::string message);
    void SendError(std::string message);
    void SendGenericError(std::string message);
    virtual std::string GetSuccessResponseName() const = 0;
    virtual std::string GetErrorResponseName() const = 0;
    virtual std::string GetRequestName() const = 0;
private:
    Server &server;

};
#endif //CLIENT_GATEWAY_H