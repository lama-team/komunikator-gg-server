#ifndef RECEIVE_FROM_CLIENT_H
#define RECEIVE_FROM_CLIENT_H
#include<string>
#include <boost/uuid/uuid.hpp>

class ReceiveFromClient{
public:
    virtual void Receive(std::string message) = 0;
};
#endif //RECEIVE_FROM_CLIENT_H