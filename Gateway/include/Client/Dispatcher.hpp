#ifndef DISPATCHER_H
#define DISPATCHER_H
#include <Model/Handler.hpp>
#include <Store/Store.hpp>
#include <Client/ClientGateway.hpp>

class Dispatcher{
public:
    Dispatcher(std::map<std::string,Handler> handlers, Store &store)
     : handlers(handlers), store(store){};
    void Receive(int fd, boost::uuids::uuid uuid, std::string messageType, 
    std::string correlationId, std::string token, std::string message);
private:
    std::map<std::string, Handler> handlers;
    Store &store;
};

#endif //DISPATCHER_H