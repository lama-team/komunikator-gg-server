#ifndef USER_SERVICE_H
#define USER_SERVICE_H
#include <string>
#include <optional>
#include <Store/Store.hpp>
#include <Utilities/Password.hpp>
#include <Model/Models.hpp>
class UserService{
public:
    UserService(Store &store) : store(store){} 
    std::optional<std::string> Login(std::string login, std::string password);
    bool Register(std::string login, std::string password);
    std::optional<std::vector<Contact>> GetContacts(ContactsQuery query);
    std::optional<ContactRequestModel> AddContactRequest(ContactRequestModel request);
    std::optional<SendMessageModel> SendMessage(SendMessageModel message);
    std::optional<std::vector<std::string>> GetCountries();
    std::optional<std::vector<SettingModel>> GetSettings(std::string userId);
    std::optional<std::vector<SettingModel>> SetSettings(std::vector<SettingModel> settings, std::string userId);
    std::optional<UserModel> SetUserDataHandler(UserModel model);
    std::optional<UserState> GetUserState(std::string userId);
    std::optional<ContactRequestConfirmModel> SetContactRequestConfirm(ContactRequestConfirmModel model);

private:
    Store &store;
};
#endif //USER_SERVICE_H