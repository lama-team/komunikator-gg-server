#ifndef STORE_H
#define STORE_H
#include <string>
#include <optional>
#include <map>
#include <Database/Session.hpp>
#include <Model/User.hpp>
#include <Model/Models.hpp>
class Store{
public:
    Store(Session &session) : session(session){};
    bool AddUser(User user);
    std::optional<User> GetUser(std::string login);
    void AddToken(std::string login, std::string token);
    std::optional<std::string> GetLogin(std::string token);
    std::optional<std::vector<Contact>> GetContacts(ContactsQuery query);
    std::optional<ContactRequestModel> AddContactRequest(ContactRequestModel request);
    std::optional<SendMessageModel> SendMessage(SendMessageModel message);
    std::optional<std::vector<std::string>> GetCountries();
    std::optional<std::vector<SettingModel>> GetSettings(std::string userId);
    std::optional<std::vector<SettingModel>> SetSettings(std::vector<SettingModel> settings, std::string userId);
    std::optional<UserModel> SetUserDataHandler(UserModel model);
    std::optional<UserState> GetUserState(std::string userId);
    std::optional<ContactRequestConfirmModel> SetContactRequestConfirm(ContactRequestConfirmModel model);
private:
    std::map<std::string, User> users;
    std::map<std::string, std::string> tokens;
    Session &session;
};
#endif //STORE_H