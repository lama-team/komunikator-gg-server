#ifndef CONTACT_REQUEST_HANDLER_H
#define CONTACT_REQUEST_HANDLER_H
#include <string>
#include <Constants/const.hpp>
#include <Client/ClientGateway.hpp>
#include <Services/UserService.hpp>

class ContactRequestHandler : public ClientGateway{
public:
    ContactRequestHandler(Server &server, UserService &userService) : ClientGateway(server), userService(userService){}
    void Receive(std::string message);
    std::string GetSuccessResponseName() const{
        return Constants::ContactRequestSuccessResponse;
    }
    std::string GetErrorResponseName() const {
        return Constants::ContactRequestErrorResponse;
    }
    std::string GetRequestName() const {
        return Constants::ContactRequestRequest;
    }
private:
    UserService &userService;
};
#endif