#ifndef LOGIN_HANDLER_H
#define LOGIN_HANDLER_H
#include <string>
#include <Constants/const.hpp>
#include <Client/ClientGateway.hpp>
#include <Services/UserService.hpp>

class LoginHandler : public ClientGateway{
public:
    LoginHandler(Server &server, UserService &userService) : ClientGateway(server), userService(userService){}
    void Receive(std::string message);
    std::string GetSuccessResponseName() const{
        return Constants::LoginSuccessResponse;
    }
    std::string GetErrorResponseName() const {
        return Constants::LoginErrorResponse;
    }
    std::string GetRequestName() const {
        return Constants::LoginRequest;
    }
private:
    UserService &userService;
};
#endif //LOGIN_HANDLER_H