#ifndef GET_USER_STATE_HANDLER_H
#define GET_USER_STATE_HANDLER_H
#include <string>
#include <Constants/const.hpp>
#include <Client/ClientGateway.hpp>
#include <Services/UserService.hpp>

class GetUserStateHandler : public ClientGateway{
public:
    GetUserStateHandler(Server &server, UserService &userService) : ClientGateway(server), userService(userService){}
    void Receive(std::string message);
    std::string GetSuccessResponseName() const{
        return Constants::GetUserStateSuccessResponse;
    }
    std::string GetErrorResponseName() const {
        return Constants::GetUserStateErrorResponse;
    }
    std::string GetRequestName() const {
        return Constants::GetUserStateRequest;
    }
private:
    UserService &userService;
};
#endif