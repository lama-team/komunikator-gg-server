#ifndef ECHO_HANDLER_H
#define ECHO_HANDLER_H
#include <string>
#include <Constants/const.hpp>
#include <Client/ClientGateway.hpp>

class EchoHandler : public ClientGateway{
public:
    using ClientGateway::ClientGateway;
    void Receive(std::string message);
    std::string GetSuccessResponseName() const{
        return Constants::EchoSuccessResponse;
    }
    std::string GetErrorResponseName() const {
        return Constants::EchoErrorResponse;
    }
    std::string GetRequestName() const {
        return Constants::EchoRequest;
    }
};
#endif //ECHO_HANDLER_H