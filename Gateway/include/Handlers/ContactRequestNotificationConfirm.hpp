#ifndef CONTACT_REQUEST_NOTIFICATION_CONFIRM_HANDLER_H
#define CONTACT_REQUEST_NOTIFICATION_CONFIRM_HANDLER_H
#include <string>
#include <Constants/const.hpp>
#include <Client/ClientGateway.hpp>
#include <Services/UserService.hpp>

class ContactRequestNotificationConfirmHandler : public ClientGateway{
public:
    ContactRequestNotificationConfirmHandler(Server &server, UserService &userService) : ClientGateway(server), userService(userService){}
    void Receive(std::string message);
    std::string GetSuccessResponseName() const{
        return Constants::ContactRequestNotificationConfirmSuccessResponse;
    }
    std::string GetErrorResponseName() const {
        return Constants::ContactRequestNotificationConfirmErrorResponse;
    }
    std::string GetRequestName() const {
        return Constants::ContactRequestNotificationConfirm;
    }
private:
    UserService &userService;
};
#endif