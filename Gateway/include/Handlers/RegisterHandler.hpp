#ifndef REGISTER_HANDLER_H
#define REGISTER_HANDLER_H
#include <string>
#include <Constants/const.hpp>
#include <Client/ClientGateway.hpp>
#include <Services/UserService.hpp>

class RegisterHandler : public ClientGateway{
public:
    RegisterHandler(Server &server, UserService &userService) : ClientGateway(server), userService(userService){}
    void Receive(std::string message);
    std::string GetSuccessResponseName() const{
        return Constants::RegisterSuccessResponse;
    }
    std::string GetErrorResponseName() const {
        return Constants::RegisterErrorResponse;
    }
    std::string GetRequestName() const {
        return Constants::RegisterRequest;
    }
private:
    UserService &userService;
};
#endif //REGISTER_HANDLER_H