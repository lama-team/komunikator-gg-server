#ifndef SET_USERDATA_HANDLER_H
#define SET_USERDATA_HANDLER_H
#include <string>
#include <Constants/const.hpp>
#include <Client/ClientGateway.hpp>
#include <Services/UserService.hpp>

class SetUserDataHandler : public ClientGateway{
public:
    SetUserDataHandler(Server &server, UserService &userService) : ClientGateway(server), userService(userService){}
    void Receive(std::string message);
    std::string GetSuccessResponseName() const{
        return Constants::SetUserDataSuccessResponse;
    }
    std::string GetErrorResponseName() const {
        return Constants::SetUserDataErrorResponse;
    }
    std::string GetRequestName() const {
        return Constants::SetUserDataRequest;
    }
private:
    UserService &userService;
};
#endif