#ifndef SET_SETTINGS_HANDLER_H
#define SET_SETTINGS_HANDLER_H
#include <string>
#include <Constants/const.hpp>
#include <Client/ClientGateway.hpp>
#include <Services/UserService.hpp>

class SetSettingsHandler : public ClientGateway{
public:
    SetSettingsHandler(Server &server, UserService &userService) : ClientGateway(server), userService(userService){}
    void Receive(std::string message);
    std::string GetSuccessResponseName() const{
        return Constants::SetSettingsSuccessResponse;
    }
    std::string GetErrorResponseName() const {
        return Constants::SetSettingsErrorResponse;
    }
    std::string GetRequestName() const {
        return Constants::SetSettingsRequest;
    }
private:
    UserService &userService;
};
#endif