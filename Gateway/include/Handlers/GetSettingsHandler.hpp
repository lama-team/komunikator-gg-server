#ifndef GET_SETTINGS_HANDLER_H
#define GET_SETTINGS_HANDLER_H
#include <string>
#include <Constants/const.hpp>
#include <Client/ClientGateway.hpp>
#include <Services/UserService.hpp>

class GetSettingsHandler : public ClientGateway{
public:
    GetSettingsHandler(Server &server, UserService &userService) : ClientGateway(server), userService(userService){}
    void Receive(std::string message);
    std::string GetSuccessResponseName() const{
        return Constants::GetSettingsSuccessResponse;
    }
    std::string GetErrorResponseName() const {
        return Constants::GetSettingsErrorResponse;
    }
    std::string GetRequestName() const {
        return Constants::GetSettingsRequest;
    }
private:
    UserService &userService;
};
#endif