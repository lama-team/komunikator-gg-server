#ifndef GET_COUNTRIES_HANDLER_H
#define GET_COUNTRIES_HANDLER_H
#include <string>
#include <Constants/const.hpp>
#include <Client/ClientGateway.hpp>
#include <Services/UserService.hpp>

class GetCountriesHandler : public ClientGateway{
public:
    GetCountriesHandler(Server &server, UserService &userService) : ClientGateway(server), userService(userService){}
    void Receive(std::string message);
    std::string GetSuccessResponseName() const{
        return Constants::GetCountriesSuccessResponse;
    }
    std::string GetErrorResponseName() const {
        return Constants::GetCountriesErrorResponse;
    }
    std::string GetRequestName() const {
        return Constants::GetCountriesRequest;
    }
private:
    UserService &userService;
};
#endif