#ifndef USER_SERVICE_IMPL_H
#define USER_SERVICE_IMPL_H
#include <grpc/grpc.h>
#include <grpc++/server.h>
#include <grpc++/server_builder.h>
#include <grpc++/server_context.h>
#include <grpc++/security/server_credentials.h>
#include <grpc++/support/sync_stream.h>
using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::ServerReader;
using grpc::ServerReaderWriter;
using grpc::ServerWriter;
using grpc::Status;
#include <messages.grpc.pb.h>
#include <Services/UserService.hpp>
#include <Utilities/Validators.hpp>
namespace komunikator{
    class UserServiceImpl final : public Communication::Service {
        public:
        UserServiceImpl(UserService &userService, Store &store) : userService(userService), store(store){}
        Status Login(ServerContext* context, const LoginRequest* request, LoginSuccessResponse* response) override{
            try{
                std::cout << "Received Login:\n" << request->DebugString() << std::endl;
                response->set_correlationid(request->correlationid());
                if(!MinLength(request->login(), 1)){
                    response->set_error("Login nie może być pusty");
                    std::cout << "Response Login:\n" << response->DebugString() << std::endl;
                    return Status::OK;
                }
                if(!MinLength(request->password(), 1)){
                    response->set_error("Hasło nie może być puste");
                    std::cout << "Response Login:\n" << response->DebugString() << std::endl;
                    return Status::OK;
                }
                auto login = request->login();
                auto password = request->password();
                auto tokenOption = userService.Login(login, password);
                if(tokenOption.has_value()){
                    response->set_token(tokenOption.value());
                } else{
                    response->set_error("Nieprawidłowe dane logowania");
                }
                std::cout << "Response Login:\n" << response->DebugString() << std::endl;
            }
            catch(std::exception const& e)
            {
                response->set_error(e.what());
                std::cout << "Response Login:\n" << response->DebugString() << std::endl;
                return Status::OK;
            }    
            return Status::OK;
        }
        Status CreateContactRequest(ServerContext* context, const ContactRequestRequest* request, ContactRequestSuccessResponse* response) override{
            try{
            std::cout << "Received ContactRequestRequest: \n" << request->DebugString() << std::endl;
            response->set_correlationid(request->correlationid());
            if(!MinLength(request->targetuserid(), 1)){
                response->set_error("targetuserid nie może być pusty");
                std::cout << "Response ContactRequestRequest:\n" << response->DebugString() << std::endl;
                return Status::OK;
            }
            if(!MinLength(request->token(), 1)){
                response->set_error("Token jest wymagany");
                std::cout << "Response ContactRequestRequest:\n" << response->DebugString() << std::endl;
                return Status::OK;
            }
            std::optional<std::string> login = store.GetLogin(request->token());
            if(login.has_value()){
                ContactRequestModel model;
                model.SourceUserId = login.value();
                model.TargetUserId = request->targetuserid();
                if(model.SourceUserId == model.TargetUserId){
                    response->set_error("Nie można wysłać zaproszenia do siebie");
                    std::cout << "Response ContactRequestRequest:\n" << response->DebugString() << std::endl;
                    return Status::OK;
                }
                auto result = userService.AddContactRequest(model);
                if(!result.has_value()){
                    response->set_error("Nie udało się dodać requestu do kontaktów");
                    std::cout << "Response ContactRequestRequest:\n" << response->DebugString() << std::endl;
                    return Status::OK;
                }
            } else{
                response->set_error("Nieprawidłowy token");
                std::cout << "Response ContactRequestRequest:\n" << response->DebugString() << std::endl;
                return Status::OK;
            }
            std::cout << "Response ContactRequestRequest:\n" << response->DebugString() << std::endl;
            }
             catch(std::exception const& e)
            {
                response->set_error(e.what());
                std::cout << "Response ContactRequestRequest:\n" << response->DebugString() << std::endl;
                return Status::OK;
            }    
            return Status::OK;
        }
        Status CreateContactRequestNotificationConfirm(ServerContext* context, const ContactRequestNotificationConfirm* request, ContactRequestNotificationConfirmSuccessResponse* response) override{
            try{
            std::cout << "Received CreateContactRequestNotificationConfirm:\n" << request->DebugString() << std::endl;
            response->set_correlationid(request->correlationid());
            if(!MinLength(request->sourceuserid(), 1)){
                response->set_error("Pusty sourceuserid");
                std::cout << "Response CreateContactRequestNotificationConfirm:\n" << response->DebugString() << std::endl;
                return Status::OK;
            }
            if(!MinLength(request->token(), 1)){
                response->set_error("Token jest wymagany");
                std::cout << "Response CreateContactRequestNotificationConfirm:\n" << response->DebugString() << std::endl;
                return Status::OK;
            }
            std::optional<std::string> login = store.GetLogin(request->token());
            if(login.has_value()){
                ContactRequestConfirmModel model;
                model.SourceUserId = request->sourceuserid();
                model.TargetUserId = login.value();
                model.Confirm = request->confirm();
                auto result = userService.SetContactRequestConfirm(model);
                if(!result.has_value()){
                    response->set_error("Nie udało się potwierdzić zaproszenia");
                    std::cout << "Response CreateContactRequestNotificationConfirm:\n" << response->DebugString() << std::endl;
                    return Status::OK;
                }
            } else{
                response->set_error("Nieprawidłowy token");
                std::cout << "Response CreateContactRequestNotificationConfirm:\n" << response->DebugString() << std::endl;
                return Status::OK;
            }
    
            std::cout << "Response CreateContactRequestNotificationConfirm:\n" << response->DebugString() << std::endl;
            return Status::OK;
            }
             catch(std::exception const& e)
            {
                response->set_error(e.what());
                std::cout << "Response CreateContactRequestNotificationConfirm:\n" << response->DebugString() << std::endl;
                return Status::OK;
            }    
        }
        Status GetContacts(ServerContext* context, const GetContactsRequest* request, GetContactsSuccessResponse* response) override{
            try{
            std::cout << "Received GetContactsRequest:\n" << request->DebugString() << std::endl;
            response->set_correlationid(request->correlationid());
            if(!MinLength(request->token(), 1)){
                response->set_error("Token jest wymagany");
                std::cout << "Response GetContactsRequest:\n" << response->DebugString() << std::endl;
                return Status::OK;
            }
            ContactsQuery query;
            query.Username = request->username();
            query.FirstName = request->firstname();
            query.LastName = request->lastname();
            query.Country= request->country();
            query.City = request->city();
            query.MinAge = request->minage();
            query.MaxAge = request->maxage();
            query.Gender = request->gender();
            auto result = userService.GetContacts(query);
            if(result.has_value()){
                auto contactsModel = result.value();
                for (auto &model : contactsModel) // access by reference to avoid copying
                {  
                    komunikator::Contact *contactModel;
                    contactModel = response->add_contacts();
                    contactModel->set_userid(model.UserId);
                    contactModel->set_username(model.Username);
                    contactModel->set_firstname(model.FirstName);
                    contactModel->set_lastname(model.LastName);
                    contactModel->set_country(model.Country);
                    contactModel->set_city(model.City);
                    contactModel->set_age(model.Age);
                    contactModel->set_gender(model.Gender);
                }
            }
            std::cout << "Response GetContactsRequest:\n" << response->DebugString() << std::endl;
            return Status::OK;
            }
             catch(std::exception const& e)
            {
                response->set_error(e.what());
                std::cout << "Response GetContactsRequest:\n" << response->DebugString() << std::endl;
                return Status::OK;
            }    
        }
        Status Echo(ServerContext* context, const EchoRequest* request, EchoSuccessResponse* response) override{
            std::cout << "Received EchoRequest:\n" << request->DebugString() << std::endl;
            response->set_correlationid(request->correlationid());
            response->set_message(request->message());
            std::cout << "Response EchoRequest:\n" << response->DebugString() << std::endl;
            return Status::OK;
        }
        Status SendMessage(ServerContext* context, const SendMessageRequest* request, SendMessageSuccessResponse* response) override{
            try{
            std::cout << "Received SendMessageRequest: \n" << request->DebugString() << std::endl;
            response->set_correlationid(request->correlationid());
            Message reqMessage;
            if(!MinLength(request->token(), 1)){
                response->set_error("Token jest wymagany");
                std::cout << "Response SendMessageRequest: \n" << response->DebugString() << std::endl;
                return Status::OK;
            }
            std::string error;
            if(request->has_message()){
                reqMessage = request->message();
                if(!MinLength(reqMessage.content(), 1)){
                    error = "Wiadomość musi mieć treść";
                }
                if(!MinLength(reqMessage.targetuserid(), 1)){
                    error = "TargetUserId puste";
                }
            } else{
                error = "Brak wiadomości";
            }
            if(MinLength(error, 1)){
                response->set_error(error);
                std::cout << "Response SendMessageRequest: \n" << response->DebugString() << std::endl;
                return Status::OK;
            }
            std::optional<std::string> login = store.GetLogin(request->token());
            if(login.has_value()){
                SendMessageModel model;
                model.SourceUserId = login.value();
                model.TargetUserId = reqMessage.targetuserid();
                model.MessageId = reqMessage.messageid();
                model.Content = reqMessage.content();
                auto result = userService.SendMessage(model);
                if(result.has_value()){
                    std::cout << "Sending message response" << std::endl;
                    auto message = result.value();
                    komunikator::Message mess;
                    mess.set_messageid(message.MessageId);
                    mess.set_content(message.Content);
                    *response->mutable_message() = mess;
                }
            } else{
                response->set_error("Nieprawidłowy token");
                std::cout << "Response SendMessageRequest: \n" << response->DebugString() << std::endl;
                return Status::OK;
            }
            std::cout << "Response SendMessageRequest: \n" << response->DebugString() << std::endl;
            return Status::OK;
            }
             catch(std::exception const& e)
            {
                response->set_error(e.what());
                std::cout << "Response SendMessageRequest: \n" << response->DebugString() << std::endl;
                return Status::OK;
            }    
        }
        Status Register(ServerContext* context, const RegisterRequest* request, RegisterSuccessResponse* response) override{
            try{
            std::cout << "Received RegisterRequest: \n" << request->DebugString() << std::endl;
            response->set_correlationid(request->correlationid());
            if(!MinLength(request->username(), 1)){
                response->set_error("Login nie może być pusty");
                std::cout << "Response RegisterRequest: \n" << response->DebugString() << std::endl;
                return Status::OK;
            }
            if(!MinLength(request->password(), 1)){
                response->set_error("Hasło nie może być puste");
                std::cout << "Response RegisterRequest: \n" << response->DebugString() << std::endl;
                return Status::OK;
            }
            auto login = request->username();
            auto password = request->password();
            if(!userService.Register(login, password)){
                response->set_error("Nie udało się zarejestrować użytkownika");
            }
            std::cout << "Response RegisterRequest: \n" << response->DebugString() << std::endl;
            return Status::OK;
            }
             catch(std::exception const& e)
            {
                response->set_error(e.what());
                std::cout << "Response RegisterRequest: \n" << response->DebugString() << std::endl;
                return Status::OK;
            }    
        }
        Status GetSettings(ServerContext* context, const GetSettingsRequest* request, GetSettingsSuccessResponse* response) override{
            try{
            std::cout << "Received GetSettingsRequest: \n" << request->DebugString() << std::endl;
            response->set_correlationid(request->correlationid());
            if(!MinLength(request->token(), 1)){
                response->set_error("Token jest wymagany");
                std::cout << "Response GetSettingsRequest: \n" << response->DebugString() << std::endl;
                return Status::OK;
            }
            std::optional<std::string> login = store.GetLogin(request->token());
            if(login.has_value()){
                auto result = userService.GetSettings(login.value());
                if(result.has_value()){
                    auto settingsModel = result.value();
                    for (auto &setting : settingsModel) // access by reference to avoid copying
                    {  
                        komunikator::Setting *settingsModel;
                        settingsModel = response->add_settings();
                        settingsModel->set_name(setting.Name);
                        settingsModel->set_value(setting.Value);
                    }
                }
            } else{
                response->set_error("Nieprawidłowy token");
                std::cout << "Response GetSettingsRequest: \n" << response->DebugString() << std::endl;
                return Status::OK;
            }
            std::cout << "Response GetSettingsRequest: \n" << response->DebugString() << std::endl;
            return Status::OK;
            }
             catch(std::exception const& e)
            {
                response->set_error(e.what());
                std::cout << "Response GetSettingsRequest: \n" << response->DebugString() << std::endl;
                return Status::OK;
            }    
        }
        Status SetSettings(ServerContext* context, const SetSettingsRequest* request, SetSettingsSuccessResponse* response) override{
            try{
            std::cout << "Received SetSettingsRequest:\n" << request->DebugString() << std::endl;
            response->set_correlationid(request->correlationid());
            if(!MinLength(request->token(), 1)){
                response->set_error("Token jest wymagany");
                std::cout << "Response SetSettingsRequest:\n" << response->DebugString() << std::endl;
                return Status::OK;
            }
            std::optional<std::string> login = store.GetLogin(request->token());
            if(login.has_value()){
                auto settingsSize = request->settings_size();
                std::string error;
                for(int i = 0; i < settingsSize; i++){
                    auto setting = request->settings(i);
                    if(!MinLength(setting.name(), 1)){
                        error = "Ustawienie nie ma klucza";
                        break;
                    }
                    if(!MinLength(setting.value(), 1)){
                        error = "Ustawienie:" + setting.name() + " nie ma klucza";
                        break;
                    }
                }
                if(MinLength(error, 1)){
                    response->set_error(error);
                    std::cout << "Response SetSettingsRequest:\n" << response->DebugString() << std::endl;
                    return Status::OK;
                }
                std::vector<SettingModel> settings;
                for(int i = 0; i < request->settings_size(); i++){
                    auto setting = request->settings(i);
                    SettingModel model;
                    model.Name = setting.name();
                    model.Value = setting.value();
                    settings.push_back(model);
                }
                auto result = userService.SetSettings(settings, login.value());
                if(result.has_value()){
                    auto settingsModel = result.value();
                    for (auto &setting : settingsModel) // access by reference to avoid copying
                    {  
                        komunikator::Setting *settingsModel;
                        settingsModel = response->add_settings();
                        settingsModel->set_name(setting.Name);
                        settingsModel->set_value(setting.Value);
                    }
                }
            } else{
                response->set_error("Nieprawidłowy token");
                std::cout << "Response SetSettingsRequest:\n" << response->DebugString() << std::endl;
                return Status::OK;
            }
            std::cout << "Response SetSettingsRequest:\n" << response->DebugString() << std::endl;
            return Status::OK;
            }
             catch(std::exception const& e)
            {
                response->set_error(e.what());
                std::cout << "Response SetSettingsRequest:\n" << response->DebugString() << std::endl;
                return Status::OK;
            }    
        }
        Status GetCountries(ServerContext* context, const GetCountriesRequest* request, GetCountriesSuccessResponse* response) override{
            try{
            std::cout << "Received GetCountriesRequest: \n" << request->DebugString() << std::endl;
            response->set_correlationid(request->correlationid());
            auto result = userService.GetCountries();
            if(result.has_value()){
                auto countries = result.value();
                for (auto &country : countries) // access by reference to avoid copying
                {  
                    response->add_countries(country);
                }
            }
            std::cout << "Response GetCountriesRequest: \n" << response->DebugString() << std::endl;
            return Status::OK;
            }
             catch(std::exception const& e)
            {
                response->set_error(e.what());
                std::cout << "Response GetCountriesRequest: \n" << response->DebugString() << std::endl;
                return Status::OK;
            }    
        }
        Status GetUserState(ServerContext* context, const GetUserStateRequest* request, GetUserStateSuccessResponse* response) override{
            try{
            //std::cout << "Received:" << request->DebugString() << std::endl;
            response->set_correlationid(request->correlationid());
            if(!MinLength(request->token(), 1)){
                response->set_error("Token jest wymagany");
                std::cout << "Response:" << response->DebugString() << std::endl;
                return Status::OK;
            }
            std::optional<std::string> login = store.GetLogin(request->token());
            if(login.has_value()){
                auto result = userService.GetUserState(login.value());
                if(result.has_value()){
                    //std::cout << "Begining to create GetUserState response" << std::endl;
                    auto userStateModel = result.value();
                    for (auto &contactRequest : userStateModel.ContactRequests) 
                    {  
                        komunikator::ContactRequest *contactRequestModel;
                        contactRequestModel = response->add_contactrequests();
                        contactRequestModel->set_sourceuserid(contactRequest.SourceUserId);
                        contactRequestModel->set_targetuserid(contactRequest.TargetUserId);
                    }
                    for (auto &contact : userStateModel.Contacts)
                    {  
                        komunikator::ContactWithMessages *contactWithMessages;
                        komunikator::Contact contactModel;
                        contactWithMessages = response->add_contacts();
                        contactModel.set_userid(contact.ContactObj.UserId);
                        contactModel.set_username(contact.ContactObj.Username);
                        contactModel.set_status(contact.ContactObj.Status);
                        contactModel.set_description(contact.ContactObj.Description);
                        //contactModel->set_avatar(contact.Username);
                        contactModel.set_firstname(contact.ContactObj.FirstName);
                        contactModel.set_lastname(contact.ContactObj.LastName);
                        contactModel.set_country(contact.ContactObj.Country);
                        contactModel.set_city(contact.ContactObj.City);
                        contactModel.set_age(contact.ContactObj.Age);
                        contactModel.set_gender(contact.ContactObj.Gender);
                        *contactWithMessages->mutable_contact() = contactModel;
                        for (auto &message : contact.Messages)
                        {  
                            komunikator::Message *messageModel;
                            messageModel = contactWithMessages->add_messages();
                            messageModel->set_messageid(message.MessageId);
                            messageModel->set_content(message.Message);
                            messageModel->set_sourceuserid(message.SourceUserId);
                            messageModel->set_targetuserid(message.TargetUserId);
                        }
                    }
                    komunikator::User user;
                    auto userModel = userStateModel.User;
                    user.set_userid(userModel.UserId);
                    user.set_username(userModel.Username);
                    user.set_status(userModel.Status);
                    user.set_description(userModel.Description);
                    //contactModel->set_avatar(userModel.Username);
                    user.set_firstname(userModel.FirstName);
                    user.set_lastname(userModel.LastName);
                    user.set_country(userModel.Country);
                    user.set_city(userModel.City);
                    user.set_age(userModel.Age);
                    user.set_gender(userModel.Gender);
                    *response->mutable_user() = user;
                }
            } else{
                response->set_error("Nieprawidłowy token");
                std::cout << "Response:" << response->DebugString() << std::endl;
                return Status::OK;
            }
            //std::cout << "Response:" << response->DebugString() << std::endl;
            return Status::OK;
            }
             catch(std::exception const& e)
            {
                response->set_error(e.what());
                std::cout << "Response:" << response->DebugString() << std::endl;
                return Status::OK;
            }    
        }
        Status SetUserData(ServerContext* context, const SetUserDataRequest* request, SetUserDataSuccessResponse* response) override{
            try{
            std::cout << "Received SetUserDataRequest: \n" << request->DebugString() << std::endl;
            response->set_correlationid(request->correlationid());
            if(!MinLength(request->token(), 1)){
                response->set_error("Token jest wymagany");
                std::cout << "Response SetUserDataRequest: \n" << response->DebugString() << std::endl;
                return Status::OK;
            }
            std::optional<std::string> login = store.GetLogin(request->token());
            if(login.has_value()){
                komunikator::User user = request->user();
                // if(user.userid() != login.value()){
                //     response->set_error("Nie można edytować danych innych użytkowników");
                //     std::cout << "Response SetUserDataRequest: \n" << response->DebugString() << std::endl;
                //     return Status::OK;
                // }
                UserModel model;
                model.UserId = login.value();
                model.Username = user.username();
                model.Status = user.status();
                model.Description = user.description();
                if(user.has_avatar()){
                    auto avatar = user.avatar();
                    if(avatar.has_image()){
                        auto image = avatar.image();
                        //model.Avatar = image.content();
                    }
                }
                model.FirstName = user.firstname();
                model.LastName = user.lastname();
                model.Country = user.country();
                model.City = user.city();
                model.Age = user.age();
                model.Gender = user.gender();
            
                auto result = userService.SetUserDataHandler(model);
                if(result.has_value()){
                    auto userModel = result.value();
                    komunikator::User user;
                    user.set_userid(userModel.UserId);
                    user.set_username(userModel.Username);
                    user.set_status(userModel.Status);
                    user.set_description(userModel.Description);
                    if(MinLength(userModel.Avatar, 1)){
                    //  user.set_avatar
                    }
                    user.set_firstname(userModel.FirstName);
                    user.set_lastname(userModel.LastName);
                    user.set_country(userModel.Country);
                    user.set_city(userModel.City);
                    user.set_age(userModel.Age);
                    user.set_gender(userModel.Gender);
                    *response->mutable_user() = user;
                }
            } else{
                response->set_error("Nieprawidłowy token");
                std::cout << "Response SetUserDataRequest: \n" << response->DebugString() << std::endl;                
                return Status::OK;
            }
            std::cout << "Response SetUserDataRequest: \n" << response->DebugString() << std::endl;
            return Status::OK;
            }
             catch(std::exception const& e)
            {
                response->set_error(e.what());
                std::cout << "Response SetUserDataRequest: \n" << response->DebugString() << std::endl;
                return Status::OK;
            }    
        }
        private:
        UserService &userService;
        Store &store;
    };
}
#endif