#ifndef GET_CONTACTS_HANDLER_H
#define GET_CONTACTS_HANDLER_H
#include <string>
#include <Constants/const.hpp>
#include <Client/ClientGateway.hpp>
#include <Services/UserService.hpp>

class GetContactsHandler : public ClientGateway{
public:
    GetContactsHandler(Server &server, UserService &userService) : ClientGateway(server), userService(userService){}
    void Receive(std::string message);
    std::string GetSuccessResponseName() const{
        return Constants::GetContactsSuccessResponse;
    }
    std::string GetErrorResponseName() const {
        return Constants::GetContactsErrorResponse;
    }
    std::string GetRequestName() const {
        return Constants::GetContactsRequest;
    }
private:
    UserService &userService;
};
#endif