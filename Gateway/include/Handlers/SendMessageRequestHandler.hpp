#ifndef SEND_MESSAGE_REQUEST_HANDLER_H
#define SEND_MESSAGE_REQUEST_HANDLER_H
#include <string>
#include <Constants/const.hpp>
#include <Client/ClientGateway.hpp>
#include <Services/UserService.hpp>

class SendMessageRequestHandler : public ClientGateway{
public:
    SendMessageRequestHandler(Server &server, UserService &userService) : ClientGateway(server), userService(userService){}
    void Receive(std::string message);
    std::string GetSuccessResponseName() const{
        return Constants::SendMessageSuccessResponse;
    }
    std::string GetErrorResponseName() const {
        return Constants::SendMessageErrorResponse;
    }
    std::string GetRequestName() const {
        return Constants::SendMessageRequest;
    }
private:
    UserService &userService;
};
#endif