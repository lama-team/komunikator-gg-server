#ifndef PASSWORD_HPP
#define PASSWORD_HPP
#include <string>
#include <sha3.h>
#include <osrng.h>
#include <cstddef>
#include <Utilities/base64.hpp>

inline std::string GenerateSalt(){
    auto range = CryptoPP::BlockingRng();
    byte salt[32];
    byte *ptr = salt;
    range.GenerateBlock(ptr, sizeof(salt));
    return base64_encode(salt, sizeof(salt));
}
inline std::string HashPassword(std::string password){
    std::vector<byte> passwordInput(password.length());
    strcpy( (char*) passwordInput.data(), password.c_str());
    byte *ptr = passwordInput.data();
    auto sha = CryptoPP::SHA3_512();
    sha.Update(ptr, passwordInput.size());
    auto digestSize = sha.DigestSize();
    std::vector<byte> passwordOutput(digestSize);
    ptr = passwordOutput.data();
    sha.Final(ptr);
    return base64_encode(ptr, passwordOutput.size());
}
#endif //PASSWORD_HPP