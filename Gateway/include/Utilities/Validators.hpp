#ifndef VALIDATORS_HPP
#define VALIDATORS_HPP
#include <string>
inline bool MinLength(std::string text, size_t length){
    return text.length() >= length;
};
#endif //VALIDATORS_HPP