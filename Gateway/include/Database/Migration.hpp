#ifndef MIGRATION_H
#define MIGRATION_H
#include <Database/Session.hpp>
#include <vector>
class Migration{
public:
    Migration(Session &sql) : sql(sql){}
    void Execute(bool dropAll, bool createAll);
    Session &sql;
private:
    std::vector<std::string> tableNames;
    bool contains(std::string tableName);
    void dropIfExists(std::string tableName);
    void fetchTableNames();
};
#endif //MIGRATION_H