#ifndef SESSION_H
#define SESSION_H
#include <string>
#include <boost/optional.hpp>
#include <ctime>
#include <oci.h>
#include <soci/soci.h>
#include <soci/oracle/soci-oracle.h>
#include <Providers/ServerConfigProvider.hpp>
using namespace soci;
extern "C" void register_factory_oracle();
class Session{
public:
    Session(ServerConfigProvider &provider) : sql("oracle", provider.GetOracleConnectionString()){
            register_factory_oracle();
    }
    session sql;
};
#endif //SESSION_H