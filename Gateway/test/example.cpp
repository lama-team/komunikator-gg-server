#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include <exception>
#include <Client/ClientGateway.hpp>
#include <Utilities/Password.hpp>


// Simple test, does not use gmock
TEST(ClientGateway, CanCreate)
{
    //ClientGateway gateway;
    //gateway.Receive() = "lol";
    //ASSERT_EQ(gateway.Receive(), "Received");
}
TEST(Password, ShouldGenerateSalt)
{
    auto salt = GenerateSalt();
}
TEST(Password, ShouldHashPassword)
{
    auto salt = GenerateSalt();
    auto hashedPassword = HashPassword("test" + salt);
    auto salt2 = GenerateSalt();
    auto hashedPassword2 = HashPassword("test" + salt);

    ASSERT_EQ(hashedPassword, hashedPassword2);
}