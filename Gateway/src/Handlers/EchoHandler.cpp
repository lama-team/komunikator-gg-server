#include <Handlers/EchoHandler.hpp>

void EchoHandler::Receive(std::string message){
    std::cout << "Parsing request" << std::endl;
    komunikator::EchoRequest request;
    std::cout << "Request: " << message << std::endl;
    request.ParseFromString(message);
    std::cout << "Request parsed" << std::endl;
    std::cout << "CorrelationId: " << request.correlationid() << std::endl;
    std::cout << "Message: " << request.message() << std::endl; 

    komunikator::EchoSuccessResponse response;
    response.set_correlationid(request.correlationid());
    response.set_message(request.message());
    *response.mutable_created() = TimeUtil::SecondsToTimestamp(time(NULL));
    std::cout << "Response creating" << std::endl;
    std::string responseString;
    response.SerializeToString(&responseString);
    std::cout << "Sending response" << std::endl;
    SendSuccess(responseString);
}