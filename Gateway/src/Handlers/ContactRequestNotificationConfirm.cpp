#include <Handlers/ContactRequestNotificationConfirm.hpp>

void ContactRequestNotificationConfirmHandler::Receive(std::string message){
    komunikator::ContactRequestNotificationConfirm request;
    std::string responseString;
    request.ParseFromString(message);
    if(!MinLength(request.sourceuserid(), 1)){
        komunikator::ContactRequestNotificationConfirmErrorResponse response;
        response.set_correlationid(to_string(requestContext.correlationId));
        response.SerializeToString(&responseString);
        SendError(responseString);
        return;
    }
    ContactRequestConfirmModel model;
    model.SourceUserId = request.sourceuserid();
    model.TargetUserId = requestContext.login.value();
    auto result = userService.SetContactRequestConfirm(model);
    if(result.has_value()){
        komunikator::ContactRequestNotificationConfirmSuccessResponse response;
        response.set_correlationid(to_string(requestContext.correlationId));
        response.SerializeToString(&responseString);
        SendSuccess(responseString);
        return;
    }
    

    komunikator::ContactRequestNotificationConfirmErrorResponse response;
    response.set_correlationid(to_string(requestContext.correlationId));
    response.SerializeToString(&responseString);
    SendError(responseString);
}