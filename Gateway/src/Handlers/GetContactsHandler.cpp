#include <Handlers/GetContactsHandler.hpp>

void GetContactsHandler::Receive(std::string message){
    komunikator::GetContactsRequest request;
    std::string responseString;
    request.ParseFromString(message);
    // if(!MinLength(request, 1)){
    //     komunikator::GetContactsErrorResponse response;
    //     response.set_correlationid(to_string(requestContext.correlationId));
    //     response.set_message("Login nie może być pusty");
    //     response.set_status(komunikator::MessageStatus::BAD_REQUEST);
    //     *response.mutable_created() = TimeUtil::GetCurrentTime();
    //     response.SerializeToString(&responseString);
    //     SendError(responseString);
    //     return;
    // }
    ContactsQuery query;
    query.Username = request.username();
    query.FirstName = request.firstname();
    query.LastName = request.lastname();
    query.Country= request.country();
    query.City = request.city();
    query.MinAge = request.minage();
    query.MaxAge = request.maxage();
    query.Gender = request.gender();
    auto result = userService.GetContacts(query);
    if(result.has_value()){
        auto contactsModel = result.value();
        komunikator::GetContactsSuccessResponse response;
        for (auto &model : contactsModel) // access by reference to avoid copying
        {  
            komunikator::Contact *contactModel;
            contactModel = response.add_contacts();
            contactModel->set_userid(model.UserId);
            contactModel->set_username(model.Username);
            contactModel->set_firstname(model.FirstName);
            contactModel->set_lastname(model.LastName);
            contactModel->set_country(model.Country);
            contactModel->set_city(model.City);
            contactModel->set_age(model.Age);
            contactModel->set_gender(model.Gender);
        }
        
        response.set_correlationid(to_string(requestContext.correlationId));
        response.SerializeToString(&responseString);
        SendSuccess(responseString);
        return;
    }
    komunikator::GetContactsErrorResponse response;
    response.set_correlationid(to_string(requestContext.correlationId));
    response.set_message("Wystąpił błąd");
    response.set_status(komunikator::MessageStatus::INTERNAL_ERROR);
    *response.mutable_created() = TimeUtil::GetCurrentTime();
    response.SerializeToString(&responseString);
    SendError(responseString);
    return;
}