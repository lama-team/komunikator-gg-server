#include <Handlers/GetUserStateHandler.hpp>

void GetUserStateHandler::Receive(std::string message){
    komunikator::GetUserStateRequest request;
    std::string responseString;
    request.ParseFromString(message);

    auto result = userService.GetUserState(requestContext.login.value());
    if(result.has_value()){
        std::cout << "Begining to create GetUserState response" << std::endl;
        auto userStateModel = result.value();
        komunikator::GetUserStateSuccessResponse response;
        for (auto &contactRequest : userStateModel.ContactRequests) 
        {  
            komunikator::ContactRequest *contactRequestModel;
            contactRequestModel = response.add_contactrequests();
            contactRequestModel->set_sourceuserid(contactRequest.SourceUserId);
            contactRequestModel->set_targetuserid(contactRequest.TargetUserId);
        }
        for (auto &contact : userStateModel.Contacts)
        {  
            komunikator::ContactWithMessages *contactWithMessages;
            komunikator::Contact contactModel;
            contactWithMessages = response.add_contacts();
            contactModel.set_userid(contact.ContactObj.UserId);
            contactModel.set_username(contact.ContactObj.Username);
            contactModel.set_status(contact.ContactObj.Status);
            contactModel.set_description(contact.ContactObj.Description);
            //contactModel->set_avatar(contact.Username);
            contactModel.set_firstname(contact.ContactObj.FirstName);
            contactModel.set_lastname(contact.ContactObj.LastName);
            contactModel.set_country(contact.ContactObj.Country);
            contactModel.set_city(contact.ContactObj.City);
            contactModel.set_age(contact.ContactObj.Age);
            contactModel.set_gender(contact.ContactObj.Gender);
            *contactWithMessages->mutable_contact() = contactModel;
            for (auto &message : contact.Messages)
            {  
                komunikator::Message *messageModel;
                messageModel = contactWithMessages->add_messages();
                messageModel->set_messageid(message.MessageId);
                messageModel->set_content(message.Message);
                messageModel->set_sourceuserid(message.SourceUserId);
                messageModel->set_targetuserid(message.TargetUserId);
            }
        }
        komunikator::User user;
        auto userModel = userStateModel.User;
        user.set_userid(userModel.UserId);
        user.set_username(userModel.Username);
        user.set_status(userModel.Status);
        user.set_description(userModel.Description);
        //contactModel->set_avatar(userModel.Username);
        user.set_firstname(userModel.FirstName);
        user.set_lastname(userModel.LastName);
        user.set_country(userModel.Country);
        user.set_city(userModel.City);
        user.set_age(userModel.Age);
        user.set_gender(userModel.Gender);
        *response.mutable_user() = user;
        response.set_correlationid(to_string(requestContext.correlationId));
        response.SerializeToString(&responseString);
        SendSuccess(responseString);
        return;
    }
    komunikator::GetUserStateErrorResponse response;
    response.set_correlationid(to_string(requestContext.correlationId));
    response.set_message("Nie można pobrać stanu użytkownika");
    response.set_status(komunikator::MessageStatus::INTERNAL_ERROR);
    *response.mutable_created() = TimeUtil::GetCurrentTime();
    response.SerializeToString(&responseString);
    SendError(responseString);
    return;
}