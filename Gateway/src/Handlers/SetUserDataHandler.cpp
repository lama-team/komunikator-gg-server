#include <Handlers/SetUserDataHandler.hpp>

void SetUserDataHandler::Receive(std::string message){
    komunikator::SetUserDataRequest request;
    std::string responseString;
    request.ParseFromString(message);
    // if(!MinLength(request, 1)){
    //     komunikator::SetUserDataErrorResponse response;
    //     response.set_correlationid(to_string(requestContext.correlationId));
    //     response.set_message("Login nie może być pusty");
    //     response.set_status(komunikator::MessageStatus::BAD_REQUEST);
    //     *response.mutable_created() = TimeUtil::GetCurrentTime();
    //     response.SerializeToString(&responseString);
    //     SendError(responseString);
    //     return;
    // }
    komunikator::User user = request.user();
    UserModel model;
    model.UserId = user.userid();
    model.Username = user.username();
    model.Status = user.status();
    model.Description = user.description();
    if(user.has_avatar()){
        auto avatar = user.avatar();
        if(avatar.has_image()){
            auto image = avatar.image();
            //model.Avatar = image.content();
        }
    }
    model.FirstName = user.firstname();
    model.LastName = user.lastname();
    model.Country = user.country();
    model.City = user.city();
    model.Age = user.age();
    model.Gender = user.gender();
   
    auto result = userService.SetUserDataHandler(model);
    if(result.has_value()){
        auto userModel = result.value();
        komunikator::User user;
        user.set_userid(userModel.UserId);
        user.set_username(userModel.Username);
        user.set_status(userModel.Status);
        user.set_description(userModel.Description);
        if(MinLength(userModel.Avatar, 1)){
          //  user.set_avatar
        }
        user.set_firstname(userModel.FirstName);
        user.set_lastname(userModel.LastName);
        user.set_country(userModel.Country);
        user.set_city(userModel.City);
        user.set_age(userModel.Age);
        user.set_gender(userModel.Gender);
        komunikator::SetUserDataSuccessResponse response;
        response.set_correlationid(to_string(requestContext.correlationId));
        *response.mutable_user() = user;
        response.SerializeToString(&responseString);
        SendSuccess(responseString);
        return;
    }
    komunikator::SetUserDataErrorResponse response;
    response.set_correlationid(to_string(requestContext.correlationId));
    response.set_message("Nie udało się ustawić danych użytkownika");
    response.set_status(komunikator::MessageStatus::INTERNAL_ERROR);
    *response.mutable_created() = TimeUtil::GetCurrentTime();
    response.SerializeToString(&responseString);
    SendError(responseString);
    return;
}