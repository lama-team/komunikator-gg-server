#include <Handlers/RegisterHandler.hpp>
void RegisterHandler::Receive(std::string message){
    std::string responseString;
    komunikator::RegisterRequest request;
    request.ParseFromString(message);
    if(!MinLength(request.username(), 1)){
        komunikator::RegisterErrorResponse response;
        response.set_correlationid(to_string(requestContext.correlationId));
        response.set_message("Login nie może być pusty");
        response.set_status(komunikator::MessageStatus::BAD_REQUEST);
        *response.mutable_created() = TimeUtil::GetCurrentTime();
        response.SerializeToString(&responseString);
        SendError(responseString);
        return;
    }
    if(!MinLength(request.password(), 1)){
        komunikator::RegisterErrorResponse response;
        response.set_correlationid(to_string(requestContext.correlationId));
        response.set_message("Hasło nie może być puste");
        response.set_status(komunikator::MessageStatus::BAD_REQUEST);
        *response.mutable_created() = TimeUtil::GetCurrentTime();
        response.SerializeToString(&responseString);
        SendError(responseString);
        return;
    }
    auto login = request.username();
    auto password = request.password();
    if(userService.Register(login, password)){
        komunikator::RegisterSuccessResponse response;
        response.set_correlationid(to_string(requestContext.correlationId));
        response.SerializeToString(&responseString);
        SendSuccess(responseString);
        return;
    }

    komunikator::RegisterErrorResponse response;
    response.set_correlationid(to_string(requestContext.correlationId));
    response.set_message("Nie udało się zarejestrować");
    response.set_status(komunikator::MessageStatus::BAD_REQUEST);
    *response.mutable_created() = TimeUtil::GetCurrentTime();
    response.SerializeToString(&responseString);
    SendError(responseString);
}