#include <Handlers/SetSettingsHandler.hpp>

void SetSettingsHandler::Receive(std::string message){
    komunikator::SetSettingsRequest request;
    std::string responseString;
    request.ParseFromString(message);
    auto settingsSize = request.settings_size();
    std::string error;
    for(int i = 0; i < settingsSize; i++){
        auto setting = request.settings(i);
        if(!MinLength(setting.name(), 1)){
            error = "Ustawienie nie ma klucza";
            break;
        }
        if(!MinLength(setting.value(), 1)){
            error = "Ustawienie:" + setting.name() + " nie ma klucza";
            break;
        }
    }
    if(MinLength(error, 1)){
        komunikator::SetSettingsErrorResponse response;
        response.set_correlationid(to_string(requestContext.correlationId));
        response.set_message(error);
        response.set_status(komunikator::MessageStatus::BAD_REQUEST);
        *response.mutable_created() = TimeUtil::GetCurrentTime();
        response.SerializeToString(&responseString);
        SendError(responseString);
        return;
    }
    std::vector<SettingModel> settings;
    for(int i = 0; i < request.settings_size(); i++){
        auto setting = request.settings(i);
        SettingModel model;
        model.Name = setting.name();
        model.Value = setting.value();
        settings.push_back(model);
    }
    auto result = userService.SetSettings(settings, requestContext.login.value());
    if(result.has_value()){
        komunikator::SetSettingsSuccessResponse response;
        auto settingsModel = result.value();
        for (auto &setting : settingsModel) // access by reference to avoid copying
        {  
            komunikator::Setting *settingsModel;
            settingsModel = response.add_settings();
            settingsModel->set_name(setting.Name);
            settingsModel->set_value(setting.Value);
        }
        response.set_correlationid(to_string(requestContext.correlationId));
        response.SerializeToString(&responseString);
        SendSuccess(responseString);
        return;
    }
    komunikator::SetSettingsErrorResponse response;
    response.set_correlationid(to_string(requestContext.correlationId));
    response.set_message("Nie udało się ustawić ustawień");
    response.set_status(komunikator::MessageStatus::INTERNAL_ERROR);
    *response.mutable_created() = TimeUtil::GetCurrentTime();
    response.SerializeToString(&responseString);
    SendError(responseString);
    return;
}