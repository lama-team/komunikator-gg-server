#include <Handlers/SendMessageRequestHandler.hpp>

void SendMessageRequestHandler::Receive(std::string message){
    komunikator::SendMessageRequest request;
    std::string responseString;
    request.ParseFromString(message);
    std::string error;
    komunikator::Message reqMessage;
    if(request.has_message()){
        reqMessage = request.message();
        if(!MinLength(reqMessage.content(), 1)){
            error = "Wiadomość musi mieć treść";
        }
        if(!MinLength(reqMessage.targetuserid(), 1)){
            error = "TargetUserId puste";
        }
    } else{
        error = "Brak wiadomości";
    }
    if(MinLength(error, 1)){
        komunikator::SendMessageErrorResponse response;
        response.set_correlationid(to_string(requestContext.correlationId));
        response.set_message(error);
        response.set_status(komunikator::MessageStatus::BAD_REQUEST);
        *response.mutable_created() = TimeUtil::GetCurrentTime();
        response.SerializeToString(&responseString);
        SendError(responseString);
        return;
    }
    SendMessageModel model;
    model.SourceUserId = requestContext.login.value();
    model.TargetUserId = reqMessage.targetuserid();
    model.MessageId = reqMessage.messageid();
    model.Content = reqMessage.content();
    auto result = userService.SendMessage(model);
    if(result.has_value()){
        std::cout << "Sending message response" << std::endl;
        auto message = result.value();
        komunikator::Message mess;
        mess.set_messageid(message.MessageId);
        mess.set_content(message.Content);
        komunikator::SendMessageSuccessResponse response;
        response.set_correlationid(to_string(requestContext.correlationId));
        *response.mutable_message() = mess;
        response.SerializeToString(&responseString);
        SendSuccess(responseString);
        return;
    }
    komunikator::SendMessageErrorResponse response;
    response.set_correlationid(to_string(requestContext.correlationId));
    response.set_message("Nie udało się wysłać wiadomości");
    response.set_status(komunikator::MessageStatus::INTERNAL_ERROR);
    *response.mutable_created() = TimeUtil::GetCurrentTime();
    response.SerializeToString(&responseString);
    SendError(responseString);
    return;
}