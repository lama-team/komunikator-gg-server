#include <Handlers/ContactRequestHandler.hpp>

void ContactRequestHandler::Receive(std::string message){
    komunikator::ContactRequestRequest request;
    std::string responseString;
    request.ParseFromString(message);
    if(!MinLength(request.targetuserid(), 1)){
        komunikator::ContactRequestErrorResponse response;
        response.set_correlationid(to_string(requestContext.correlationId));
        response.set_message("targetuserid nie może być pusty");
        response.set_status(komunikator::MessageStatus::BAD_REQUEST);
        *response.mutable_created() = TimeUtil::GetCurrentTime();
        response.SerializeToString(&responseString);
        SendError(responseString);
        return;
    }
    ContactRequestModel model;
    model.SourceUserId = requestContext.login.value();
    model.TargetUserId = request.targetuserid();
    auto result = userService.AddContactRequest(model);
    if(result.has_value()){
        komunikator::ContactRequestSuccessResponse response;
        response.set_correlationid(to_string(requestContext.correlationId));
        response.SerializeToString(&responseString);
        SendSuccess(responseString);
        return;
    }

    komunikator::ContactRequestErrorResponse response;
    response.set_correlationid(to_string(requestContext.correlationId));
    response.set_message("nie udało się dodać contact requestu");
    response.set_status(komunikator::MessageStatus::INTERNAL_ERROR);
    *response.mutable_created() = TimeUtil::GetCurrentTime();
    response.SerializeToString(&responseString);
    SendError(responseString);
}