#include <Handlers/LoginHandler.hpp>
void LoginHandler::Receive(std::string message){
    komunikator::LoginRequest request;
    std::string responseString;
    request.ParseFromString(message);
    if(!MinLength(request.login(), 1)){
        komunikator::LoginErrorResponse response;
        response.set_correlationid(to_string(requestContext.correlationId));
        response.set_message("Login nie może być pusty");
        response.set_status(komunikator::MessageStatus::BAD_REQUEST);
        *response.mutable_created() = TimeUtil::GetCurrentTime();
        response.SerializeToString(&responseString);
        SendError(responseString);
        return;
    }
    if(!MinLength(request.password(), 1)){
        komunikator::LoginErrorResponse response;
        response.set_correlationid(to_string(requestContext.correlationId));
        response.set_message("Hasło nie może być puste");
        response.set_status(komunikator::MessageStatus::BAD_REQUEST);
        *response.mutable_created() = TimeUtil::GetCurrentTime();
        response.SerializeToString(&responseString);
        SendError(responseString);
        return;
    }
    auto login = request.login();
    auto password = request.password();
    auto tokenOption = userService.Login(login, password);
    if(tokenOption.has_value()){
        komunikator::LoginSuccessResponse response;
        response.set_correlationid(to_string(requestContext.correlationId));
        response.set_token(tokenOption.value());
        response.SerializeToString(&responseString);
        SendSuccess(responseString);
        return;
    }

    komunikator::LoginErrorResponse response;
    response.set_correlationid(to_string(requestContext.correlationId));
    response.set_message("Nieprawidłowe dane logowania");
    response.set_status(komunikator::MessageStatus::BAD_REQUEST);
    *response.mutable_created() = TimeUtil::GetCurrentTime();
    response.SerializeToString(&responseString);
    SendError(responseString);
  
}