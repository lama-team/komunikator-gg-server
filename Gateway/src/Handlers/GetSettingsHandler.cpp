#include <Handlers/GetSettingsHandler.hpp>

void GetSettingsHandler::Receive(std::string message){
    komunikator::GetSettingsRequest request;
    std::string responseString;
    request.ParseFromString(message);
    // if(!MinLength(request, 1)){
    //     komunikator::GetSettingsErrorResponse response;
    //     response.set_correlationid(to_string(requestContext.correlationId));
    //     response.set_message("Login nie może być pusty");
    //     response.set_status(komunikator::MessageStatus::BAD_REQUEST);
    //     *response.mutable_created() = TimeUtil::GetCurrentTime();
    //     response.SerializeToString(&responseString);
    //     SendError(responseString);
    //     return;
    // }
    auto result = userService.GetSettings(requestContext.login.value());
    if(result.has_value()){
        auto settingsModel = result.value();
        komunikator::GetSettingsSuccessResponse response;
        for (auto &setting : settingsModel) // access by reference to avoid copying
        {  
            komunikator::Setting *settingsModel;
            settingsModel = response.add_settings();
            settingsModel->set_name(setting.Name);
            settingsModel->set_value(setting.Value);
        }
        response.set_correlationid(to_string(requestContext.correlationId));
        response.SerializeToString(&responseString);
        SendSuccess(responseString);
        return;
    }
    komunikator::GetSettingsErrorResponse response;
    response.set_correlationid(to_string(requestContext.correlationId));
    response.set_message("Nie można pobrać ustawień");
    response.set_status(komunikator::MessageStatus::INTERNAL_ERROR);
    *response.mutable_created() = TimeUtil::GetCurrentTime();
    response.SerializeToString(&responseString);
    SendError(responseString);
    return;
}