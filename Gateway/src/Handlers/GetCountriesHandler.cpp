#include <Handlers/GetCountriesHandler.hpp>

void GetCountriesHandler::Receive(std::string message){
    komunikator::GetCountriesRequest request;
    std::string responseString;
    request.ParseFromString(message);
    // if(!MinLength(request, 1)){
    //     komunikator::GetContactsErrorResponse response;
    //     response.set_correlationid(to_string(requestContext.correlationId));
    //     response.set_message("Login nie może być pusty");
    //     response.set_status(komunikator::MessageStatus::BAD_REQUEST);
    //     *response.mutable_created() = TimeUtil::GetCurrentTime();
    //     response.SerializeToString(&responseString);
    //     SendError(responseString);
    //     return;
    // }
    auto result = userService.GetCountries();
    if(result.has_value()){
        auto countries = result.value();
        komunikator::GetCountriesSuccessResponse response;
        for (auto &country : countries) // access by reference to avoid copying
        {  
            response.add_countries(country);
        }
        response.set_correlationid(to_string(requestContext.correlationId));
        response.SerializeToString(&responseString);
        SendSuccess(responseString);
        return;
    }
    komunikator::GetContactsErrorResponse response;
    response.set_correlationid(to_string(requestContext.correlationId));
    response.set_message("Nie można pobrać krajów");
    response.set_status(komunikator::MessageStatus::INTERNAL_ERROR);
    *response.mutable_created() = TimeUtil::GetCurrentTime();
    response.SerializeToString(&responseString);
    SendError(responseString);
    return;
}