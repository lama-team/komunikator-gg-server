#include <Database/Migration.hpp>
#include <iostream>
#include <algorithm>
#include <boost/algorithm/string/predicate.hpp>
#include <fstream>
#include <sstream>

bool Migration::contains(std::string tableName){
    const auto it = std::find_if(
        std::begin(tableNames),
        std::end(tableNames),
        [&tableName](const auto& str) { return boost::iequals(tableName, str); }
        );
    const bool result = it != std::end(tableNames);
    //std::cout << tableName << " exists: " << result << std::endl;
    return result;
}
void Migration::dropIfExists(std::string tableName){
    if(contains(tableName)) {
        std::cout << "Droping table " << tableName << std::endl;
        sql.sql << "DROP TABLE " << tableName;
    }
}

void Migration::fetchTableNames(){
    int count;
    sql.sql << "select count(*) from user_tables", into(count);
    tableNames.resize(count);
    sql.sql << "select TABLE_NAME from user_tables", into (tableNames);
}

void Migration::Execute(bool dropAll, bool createAll){
    std::cout << "Migrating database" << std::endl;
    fetchTableNames();
    sql.sql.begin();
    if(dropAll){
        dropIfExists("useravatars");
        dropIfExists("contacts");
        dropIfExists("contactRequests");
        dropIfExists("descriptions");
        dropIfExists("messages");
        dropIfExists("settings");
        dropIfExists("users");
        dropIfExists("countries");
    }
    fetchTableNames();
    if(createAll){
        if (!contains("countries"))
        {
            std::cout << "Creating table " << "countries" << std::endl;
            sql.sql << "CREATE TABLE countries("
                    << "name VARCHAR2(100) NOT NULL"
                    << ")";
            sql.sql << "ALTER TABLE countries ADD CONSTRAINT countries_pk PRIMARY KEY(name)";
            std::ifstream file("Gateway/files/panstwa");
            std::string linebuffer;
            statement st = (sql.sql.prepare <<
                "INSERT INTO countries(name) VALUES(:val)",
                use(linebuffer));
            while (file && getline(file, linebuffer)){
                if (linebuffer.length() == 0)continue;
                st.execute(true);
            }
        }
        if (!contains("users")) //not contains
        {
            std::cout << "Creating table " << "users" << std::endl;
            sql.sql << "CREATE TABLE users ("
                    << "id VARCHAR2(100) NOT NULL,"
                    << "hashedpassword VARCHAR2(128) NOT NULL,"
                    << "salt VARCHAR2(64) NOT NULL,"
                    << "firstname VARCHAR2(50),"
                    << "lastname VARCHAR2(50),"
                    << "age NUMBER,"
                    << "gender VARCHAR2(1),"
                    << "city VARCHAR2(100),"
                    << "countries_name VARCHAR2(100)"
                    << ")";
            sql.sql << "ALTER TABLE users ADD CONSTRAINT user_pk PRIMARY KEY (id)";
            sql.sql << "ALTER TABLE users"
                    << "    ADD CONSTRAINT users_countries_fk FOREIGN KEY (countries_name)"
                    << "        REFERENCES countries(name)";
        }
        if (!contains("useravatars")){
            std::cout << "Creating table " << "useravatars" << std::endl;
            sql.sql << "CREATE TABLE useravatars ("
                    << "avatar BLOB NOT NULL,"
                    << "users_id VARCHAR2(36) NOT NULL"
                    << ")";
            sql.sql << "ALTER TABLE useravatars ADD CONSTRAINT useravatars_pk PRIMARY KEY (users_id)";
            sql.sql << "ALTER TABLE useravatars"
                    << "    ADD CONSTRAINT useravatars_users_fk FOREIGN KEY (users_id)"
                    << "        REFERENCES users(id)";
        }
        if (!contains("contacts")){
            std::cout << "Creating table " << "contacts" << std::endl;
            sql.sql << "CREATE TABLE contacts ("
                    << "source_user VARCHAR2(36) NOT NULL,"
                    << "target_user VARCHAR2(36) NOT NULL"
                    << ")";
            sql.sql << "ALTER TABLE contacts ADD CONSTRAINT contacts_pk PRIMARY KEY (source_user, target_user)";
            sql.sql << "ALTER TABLE contacts"
                    << "    ADD CONSTRAINT contacts_users_fk FOREIGN KEY (source_user)"
                    << "        REFERENCES users(id)";
            sql.sql << "ALTER TABLE contacts"
                    << "    ADD CONSTRAINT contacts_users_fkv1 FOREIGN KEY (target_user)"
                    << "        REFERENCES users(id)";
        }
        if (!contains("contactRequests")){
            std::cout << "Creating table " << "contactRequests" << std::endl;
            sql.sql << "CREATE TABLE contactRequests ("
                    << "source_user VARCHAR2(36) NOT NULL,"
                    << "target_user VARCHAR2(36) NOT NULL,"
                    << "accepted VARCHAR2(1)"
                    << ")";
            sql.sql << "ALTER TABLE contactRequests ADD CONSTRAINT contactRequests_pk PRIMARY KEY (source_user, target_user)";
            sql.sql << "ALTER TABLE contactRequests"
                    << "    ADD CONSTRAINT contactRequests_users_fk FOREIGN KEY (source_user)"
                    << "        REFERENCES users(id)";
            sql.sql << "ALTER TABLE contactRequests"
                    << "    ADD CONSTRAINT contactRequests_users_fkv1 FOREIGN KEY (target_user)"
                    << "        REFERENCES users(id)";
        }
        if (!contains("descriptions")){
            std::cout << "Creating table " << "descriptions" << std::endl;
            sql.sql << "CREATE TABLE descriptions ("
                    << "changed DATE NOT NULL,"
                    << "description VARCHAR2(100),"
                    << "users_id VARCHAR2(36) NOT NULL"
                    << ")";
            sql.sql << "ALTER TABLE descriptions ADD CONSTRAINT descriptions_pk PRIMARY KEY ( changed,users_id )";
            sql.sql << "ALTER TABLE descriptions"
                    << "    ADD CONSTRAINT descriptions_users_fk FOREIGN KEY ( users_id )"
                    << "        REFERENCES users ( id )";
        }
        if (!contains("messages")){
            std::cout << "Creating table " << "messages" << std::endl;
            sql.sql << "CREATE TABLE messages ("
                    << "id VARCHAR2(36) NOT NULL,"
                    << "message VARCHAR2(2000) NOT NULL,"
                    << "created DATE NOT NULL,"
                    << "source_user VARCHAR2(36) NOT NULL,"
                    << "target_user VARCHAR2(36) NOT NULL,"
                    << "read VARCHAR2(1) NOT NULL"
                    << ")";
            sql.sql << "ALTER TABLE messages ADD CONSTRAINT messages_pk PRIMARY KEY ( id )";
            sql.sql << "ALTER TABLE messages"
                    << "    ADD CONSTRAINT messages_users_fk FOREIGN KEY (source_user)"
                    << "        REFERENCES users(id)";
            sql.sql << "ALTER TABLE messages"
                    << "    ADD CONSTRAINT messages_users_fkv1 FOREIGN KEY (target_user)"
                    << "        REFERENCES users(id)";
        }
        if (!contains("settings")){
            std::cout << "Creating table " << "settings" << std::endl;
            sql.sql << "CREATE TABLE settings ("
                    << "name VARCHAR2(100) NOT NULL,"
                    << "value VARCHAR2(100) NOT NULL,"
                    << "users_id VARCHAR2(36) NOT NULL"
                    << ")";
            sql.sql << "ALTER TABLE settings ADD CONSTRAINT settings_pk PRIMARY KEY ( name, value, users_id )";
            sql.sql << "ALTER TABLE settings"
                    << "    ADD CONSTRAINT settings_users_fk FOREIGN KEY (users_id)"
                    << "        REFERENCES users(id)";
        }
    }
    sql.sql.commit();
    std::cout << "Migration succesful" << std::endl;
}