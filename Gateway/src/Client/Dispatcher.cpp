#include <Client/Dispatcher.hpp>
#include <soci/soci.h>
#include <soci/oracle/soci-oracle.h>
#include <Utilities/Validators.hpp>
#include <Constants/const.hpp>
void Dispatcher::Receive(int fd, boost::uuids::uuid uuid, std::string messageType, 
    std::string correlationId, std::string token, std::string message)
{
    std::cout << "Checking if handler exist for: " << messageType << std::endl;
    if (!(this->handlers.count(messageType))){
        std::cout << "Handler not found for: " << messageType << std::endl;
        komunikator::GenericError response;
        response.set_correlationid(correlationId);
        response.set_messagetype("Handler not found: " + messageType);
        *response.mutable_created() = TimeUtil::SecondsToTimestamp(time(NULL));
        response.set_status(komunikator::MessageStatus::INTERNAL_ERROR);
        std::string responseString;
        response.SerializeToString(&responseString);
        this->handlers[Constants::EchoRequest].handler->SendGenericError(responseString);
        return;
    }
    Handler handler = this->handlers[messageType];
    std::cout << "handler found for: " << messageType << std::endl;
    Request request;
    request.fd = fd;
    request.uuid = uuid;
    boost::uuids::string_generator gen;
    std::cout << "setting correlationId for: " << messageType << std::endl;
    request.correlationId = gen(correlationId);
    std::cout << "setting request context for: " << messageType << std::endl;
    handler.handler->requestContext = request;
    std::cout << "checking if auth required for: " << messageType << std::endl;
    if(handler.requiresAuthentication){
        std::cout << "auth required, checking token for: " << messageType << std::endl;
        if(!MinLength(token, 1)){
            std::cout << "token not found for: " << messageType << std::endl;
            komunikator::GenericError response;
            response.set_correlationid(correlationId);
            response.set_messagetype(handler.handler->GetErrorResponseName());
            *response.mutable_created() = TimeUtil::SecondsToTimestamp(time(NULL));
            response.set_status(komunikator::MessageStatus::UNAUTHORIZED);
            std::string responseString;
            response.SerializeToString(&responseString);
            handler.handler->SendGenericError(responseString);
            return;
        }
        std::cout << "Verifying token for: " << messageType << " , token: " << token << std::endl;
        std::optional<std::string> login = store.GetLogin(token);
        if(!login.has_value()){
            komunikator::GenericError response;
            response.set_correlationid(correlationId);
            response.set_messagetype(handler.handler->GetErrorResponseName());
            *response.mutable_created() = TimeUtil::SecondsToTimestamp(time(NULL));
            response.set_status(komunikator::MessageStatus::UNAUTHORIZED);
            std::string responseString;
            response.SerializeToString(&responseString);
            handler.handler->SendGenericError(responseString);
            return;
        }
        request.login = login;
    }
    std::cout << "setting request context for: " << messageType << std::endl;
    handler.handler->requestContext = request;
    try
    {
        std::cout << "Entering handler for: " << messageType << std::endl;
        handler.handler->Receive(message);
    }
    catch (soci::oracle_soci_error const & e)
    {
        std::cerr << "Oracle error: " << e.err_num_
            << " " << e.what() << std::endl;
        std::cerr << "Correlation id:" << correlationId << std::endl;
        std::cerr << "Exception ocurred on: " << messageType << std::endl;
        std::cerr << "Message body:" << message << std::endl;
        komunikator::GenericError response;
        response.set_correlationid(correlationId);
        response.set_messagetype(handler.handler->GetErrorResponseName());
        *response.mutable_created() = TimeUtil::SecondsToTimestamp(time(NULL));
        komunikator::Exception exception;
        exception.set_type("INTERNAL_ERROR");
        exception.set_message(e.what());
        *response.mutable_exception() = exception;
        response.set_status(komunikator::MessageStatus::INTERNAL_ERROR);
        std::string responseString;
        response.SerializeToString(&responseString);
        handler.handler->SendGenericError(responseString);
    }
    catch(std::runtime_error& e)
    {
        std::cerr << "Correlation id:" << correlationId << std::endl;
        std::cerr << "Exception ocurred on: " << messageType << std::endl;
        std::cerr << e.what() << "\n";
        std::cerr << "Message body:" << message << std::endl;
        komunikator::GenericError response;
        response.set_correlationid(correlationId);
        response.set_messagetype(handler.handler->GetErrorResponseName());
        *response.mutable_created() = TimeUtil::SecondsToTimestamp(time(NULL));
        komunikator::Exception exception;
        exception.set_type("INTERNAL_ERROR");
        exception.set_message("Internal error");
        *response.mutable_exception() = exception;
        response.set_status(komunikator::MessageStatus::INTERNAL_ERROR);
        std::string responseString;
        response.SerializeToString(&responseString);
        handler.handler->SendGenericError(responseString);
    }
    catch(std::exception const& e)
    {
        std::cerr << "Correlation id:" << correlationId << std::endl;
        std::cerr << "Exception ocurred on: " << messageType << std::endl;
        std::cerr << e.what() << "\n";
        std::cerr << "Message body:" << message << std::endl;
        komunikator::GenericError response;
        response.set_correlationid(correlationId);
        response.set_messagetype(handler.handler->GetErrorResponseName());
        *response.mutable_created() = TimeUtil::SecondsToTimestamp(time(NULL));
        komunikator::Exception exception;
        exception.set_type("INTERNAL_ERROR");
        exception.set_message("Internal error");
        *response.mutable_exception() = exception;
        response.set_status(komunikator::MessageStatus::INTERNAL_ERROR);
        std::string responseString;
        response.SerializeToString(&responseString);
        handler.handler->SendGenericError(responseString);
    }
    catch (...)
    {
        std::cerr << "Correlation id:" << correlationId << std::endl;
        std::cerr << "Exception ocurred on: " << messageType << std::endl;
        std::cerr << "Message body:" << message << std::endl;
        komunikator::GenericError response;
        response.set_correlationid(correlationId);
        response.set_messagetype(handler.handler->GetErrorResponseName());
        *response.mutable_created() = TimeUtil::SecondsToTimestamp(time(NULL));
        komunikator::Exception exception;
        exception.set_type("INTERNAL_ERROR");
        exception.set_message("Internal error");
        *response.mutable_exception() = exception;
        response.set_status(komunikator::MessageStatus::INTERNAL_ERROR);
        std::string responseString;
        response.SerializeToString(&responseString);
        handler.handler->SendGenericError(responseString);
    }
}