#include <Client/ClientGateway.hpp>

void ClientGateway::Send(std::string message) const
{
    std::cout << "Request.fd: " << requestContext.fd << std::endl;
    std::cout << "Request uuid: " << to_string(requestContext.uuid) << std::endl;
    server.Send(requestContext.fd, requestContext.uuid, message);
}

void ClientGateway::SendSuccess(std::string message)
{
    std::string responseMessageType = GetSuccessResponseName();
    int len = responseMessageType.length();
    std::string responseMessage = "";
    responseMessage += (char)len;
    responseMessage += responseMessageType;
    int bodyLen = message.length();
    responseMessage += (char)(bodyLen >> 24) & 0xFF;
    responseMessage += (char)(bodyLen >> 16) & 0xFF;
    responseMessage += (char)(bodyLen >> 8) & 0xFF;
    responseMessage += (char) bodyLen & 0xFF;
    responseMessage += message;
    Send(responseMessage);
}
void ClientGateway::SendError(std::string message)
{
    std::string responseMessageType = GetErrorResponseName();
    int len = responseMessageType.length();
    std::string responseMessage = "";
    responseMessage += (char)len;
    responseMessage += responseMessageType;
    int bodyLen = message.length();
    responseMessage += (char)(bodyLen >> 24) & 0xFF;
    responseMessage += (char)(bodyLen >> 16) & 0xFF;
    responseMessage += (char)(bodyLen >> 8) & 0xFF;
    responseMessage += (char) bodyLen & 0xFF;
    responseMessage += message;
    Send(responseMessage);
}
void ClientGateway::SendGenericError(std::string message)
{
    std::string responseMessageType = "GENERIC_ERROR";
    int len = responseMessageType.length();
    std::string responseMessage = "";
    responseMessage += (char)len;
    responseMessage += responseMessageType;
    int bodyLen = message.length();
    responseMessage += (char)(bodyLen >> 24) & 0xFF;
    responseMessage += (char)(bodyLen >> 16) & 0xFF;
    responseMessage += (char)(bodyLen >> 8) & 0xFF;
    responseMessage += (char) bodyLen & 0xFF;
    responseMessage += message;
    Send(responseMessage);
}