#include <Store/Store.hpp>
#include <soci/soci.h>
#include <soci/oracle/soci-oracle.h>
#include <Utilities/Validators.hpp>
#include <ctime>

bool Store::AddUser(User user){
    // if(users.count(user.Login) > 0){
    //     return false;
    // }
    // users[user.Login] = user;
    // return true;
    int count;
    session.sql << "SELECT count(id) FROM USERS WHERE id = '" << user.Id << "'", into(count);
    if(count > 0){
        return false;
    } else{
        session.sql << "INSERT INTO users(id, hashedpassword,salt) VALUES(:id, :hashedpassword, :salt)", use(user.Id), use(user.HashedPassword), use(user.Salt);
        session.sql.commit();
        return true;
    }
}
std::optional<User> Store::GetUser(std::string login){
    // if(users.count(login) > 0){
    //     return std::optional<User>{users[login]};
    // }
    // return std::nullopt;
    std::cout << "Checking login: " << login << std::endl;
    User user;
    session.sql << "SELECT id, hashedpassword, salt FROM users WHERE id = '" << login << "'", into(user.Id), into(user.HashedPassword), into(user.Salt);
    std::cout << "id: " << user.Id << std::endl;
    if(user.Id == login){
        std::cout << "Login found, password: " << user.HashedPassword << std::endl;
        return std::optional<User>{user};
    }
    
    std::cout << "Login not found" << std::endl;
    return std::nullopt;
}

void Store::AddToken(std::string login, std::string token){
    tokens[token] = login;
}
std::optional<std::string> Store::GetLogin(std::string token){
    if(tokens.count(token) > 0){
        return std::optional<std::string>{tokens[token]};
    }
    return std::nullopt;
}

std::optional<std::vector<Contact>> Store::GetContacts(ContactsQuery query){
    std::string sql = "";
    if(MinLength(query.Username, 1)){
        sql += "id = '" + query.Username + "' ";
    }
    if(MinLength(query.FirstName,1)){
        if(MinLength(sql, 1)){
            sql += "AND ";
        }
        sql += "firstname = '" + query.FirstName + "' ";
    }
    if(MinLength(query.LastName,1)){
        if(MinLength(sql, 1)){
            sql += "AND ";
        }
        sql += "lastname = '" + query.LastName + "' ";
    }
    if(MinLength(query.Country,1)){
        if(MinLength(sql, 1)){
            sql += "AND ";
        }
        sql += "country = '" + query.Country + "' ";
    }
    if(MinLength(query.City,1)){
        if(MinLength(sql, 1)){
            sql += "AND ";
        }
        sql += "city = '" + query.City + "' ";
    }
    if(query.MinAge > 0){
        if(MinLength(sql, 1)){
            sql += "AND ";
        }
        sql += "age >= '" + std::to_string(query.MinAge) + "' ";
    }
    if(query.MaxAge > 0){
        if(MinLength(sql, 1)){
            sql += "AND ";
        }
        sql += "age <= '" + std::to_string(query.MaxAge) + "' ";
    }
    if(MinLength(query.City,1)){
        if(MinLength(sql, 1)){
            sql += "AND ";
        }
        sql += "gender = '" + query.Gender + "' ";
    }
    if(MinLength(sql, 1)){
        sql = "WHERE " + sql;
    }
    std::vector<Contact> contacts;
    User user;
    indicator ind;
    statement st = (session.sql.prepare << "SELECT id, firstname, lastname, age, gender, city, countries_name FROM "
    << "users " << sql, into(user.Id,ind), into(user.FirstName,ind),
    into(user.LastName,ind), into(user.Age,ind), into(user.Gender,ind), into(user.City,ind),
    into(user.Country,ind));
    st.execute();
    while (st.fetch())
    {
        Contact model;
        model.UserId = user.Id;
        model.Username = user.Id;
        model.FirstName = user.FirstName;
        model.LastName = user.LastName;
        model.Age = user.Age;
        model.Gender = user.Gender;
        model.City = user.City;
        model.Country = user.Country;
        contacts.push_back(model);
    }

    return std::optional<std::vector<Contact>>{contacts};
    
}

std::optional<ContactRequestModel> Store::AddContactRequest(ContactRequestModel request){
    int count;
    session.sql << "SELECT count(*) FROM contactRequests WHERE accepted IS NULL AND source_user = '" << request.SourceUserId << "' "
    << "AND target_user = '" << request.TargetUserId << "'", into(count);
    if(count > 0) throw std::runtime_error(("Zaproszenie już istnieje"));
    session.sql << "SELECT count(*) FROM contactRequests WHERE accepted IS NULL AND target_user = '" << request.SourceUserId << "' "
    << "AND source_user = '" << request.TargetUserId << "'", into(count);
    if(count > 0) throw std::runtime_error(("Zaproszenie już istnieje"));
    session.sql << "SELECT count(*) FROM users WHERE id = '" << request.SourceUserId << "' ", into(count);
    if(count == 0) throw std::runtime_error(("Użytkownik " + request.SourceUserId + " nie istnieje"));
    session.sql << "SELECT count(*) FROM users WHERE id = '" << request.TargetUserId << "' ", into(count);
    if(count == 0) throw std::runtime_error(("Użytkownik " + request.TargetUserId + " nie istnieje"));
    session.sql << "SELECT count(*) FROM contacts WHERE source_user = '" << request.SourceUserId << "' AND "
    << "target_user = '" << request.TargetUserId << "'", into(count);
    if(count != 0) throw std::runtime_error(("Użytkownik " + request.TargetUserId + " jest już dodany do kontaktów"));
    session.sql << "INSERT INTO contactRequests(source_user, target_user) "
    << "VALUES(:source_user, :target_user)", use(request.SourceUserId), use(request.TargetUserId);
    session.sql.commit();
    return std::optional<ContactRequestModel>{request};
    
}
std::optional<SendMessageModel> Store::SendMessage(SendMessageModel message){
    // current date/time based on current system
    time_t now = time(0);
    tm *ltm = localtime(&now);
    int count;
    session.sql << "SELECT count(*) FROM users WHERE id = '" << message.SourceUserId << "' ", into(count);
    if(count == 0) throw std::runtime_error(("Użytkownik " + message.SourceUserId + " nie istnieje"));
    session.sql << "SELECT count(*) FROM users WHERE id = '" << message.TargetUserId << "' ", into(count);
    if(count == 0) throw std::runtime_error(("Użytkownik " + message.TargetUserId + " nie istnieje"));
    session.sql << "INSERT INTO messages(id, message, created, source_user, target_user, read) "
        << "VALUES(:id, :message, :created, :source_user, :target_user, 'N')", use(message.MessageId), use(message.Content), use(*ltm), use(message.SourceUserId), use(message.TargetUserId);
    session.sql.commit();
    return std::optional<SendMessageModel>{message};
}
std::optional<std::vector<std::string>> Store::GetCountries(){
    std::vector<std::string> countries(500);
    session.sql << "SELECT name FROM countries", into(countries);
    countries.shrink_to_fit();
    return std::optional<std::vector<std::string>>{countries};
}
std::optional<std::vector<SettingModel>> Store::GetSettings(std::string userId){
    std::vector<SettingModel> settings;
    SettingModel model;
    statement st = (session.sql.prepare << "SELECT name, value FROM "
    << "settings WHERE users_id = '" << userId << "'", into(model.Name), into(model.Value));
    st.execute();
    while (st.fetch())
    {
        settings.push_back(model);
    }
    return std::optional<std::vector<SettingModel>>{settings};
}
std::optional<std::vector<SettingModel>> Store::SetSettings(std::vector<SettingModel> settings, std::string userId){
    session.sql << "DELETE FROM settings WHERE users_id = '" + userId + "'";
    std::string name;
    std::string value;
    statement st = (session.sql.prepare << "insert into settings(name, value, users_id) values(:name, :value, :id)", use(name), use(value), use(userId));
    for (auto &setting : settings) // access by reference to avoid copying
    {  
        name = setting.Name;
        value = setting.Value;
        st.execute(true);
    }
    session.sql.commit();
    return std::optional<std::vector<SettingModel>>{settings};
}
std::optional<UserModel> Store::SetUserDataHandler(UserModel model){
    session.sql << "UPDATE users SET firstname = :firstname , lastname = :lastname , "
    << "countries_name = :countries_name, city = :city, age = :age , gender = :gender WHERE id = '"
    << model.UserId << "'", use(model.FirstName), use(model.LastName), use(model.Country),
    use(model.City), use(model.Age), use(model.Gender);
    session.sql.commit();
    return std::optional<UserModel>{model};
}

std::optional<UserState> Store::GetUserState(std::string userId){
    UserModel user;
    indicator ind;
    indicator ageInd;
    //std::cout << "selecting user" << std::endl;
    session.sql << "SELECT id, firstname, lastname, age, gender, city, countries_name FROM users WHERE id = '"
    << userId << "'", into(user.UserId), into(user.FirstName, ind),
    into(user.LastName, ind), into(user.Age, ageInd), into(user.Gender, ind), into(user.City, ind),
    into(user.Country, ind);
    if(ageInd == i_null) user.Age = 0;
    user.Username = user.UserId;
    std::vector<ContactWithMessages> contacts;
    Contact model;
    //std::cout << "selecting contacts" << std::endl;
    //std::cout << "userid: " << userId << std::endl;
    statement st = (session.sql.prepare << "SELECT users.id, users.firstname, users.lastname, users.lastname, users.age, users.gender, users.city, users.countries_name FROM "
    << "contacts INNER JOIN users ON users.id = contacts.source_user WHERE contacts.target_user = '" << userId << "'", into(model.UserId), into(model.FirstName, ind),
    into(model.LastName, ind), into(model.Age, ageInd), into(model.Gender, ind), into(model.City, ind),into(model.Country, ind));
    st.execute();
    while (st.fetch())
    {
        ContactWithMessages contactWithMessages;
        model.Username = model.UserId;
        if(ageInd == i_null) model.Age = 0;
        contactWithMessages.ContactObj = model;
        contacts.push_back(contactWithMessages);
    }
    Message message;
    //std::cout << "selecting messages" << std::endl;
    for (auto &contact : contacts) 
    {  
        statement st2 = (session.sql.prepare << "SELECT id, message, created, source_user, target_user, read FROM "
        << "messages WHERE (target_user = '" << userId << "' AND source_user='" << contact.ContactObj.UserId << "') OR (target_user= '" 
        << contact.ContactObj.UserId << "' AND source_user= '" << userId <<"')", into(message.MessageId), into(message.Message),
        into(message.Created), into(message.SourceUserId), into(message.TargetUserId));
        st2.execute();
        while (st2.fetch())
        {
            contact.Messages.push_back(message);
        }
    }
    //std::cout << "selecting contact requests" << std::endl;
    std::vector<ContactRequest> contactRequests;
    ContactRequest contactRequest;
    statement st3 = (session.sql.prepare << "SELECT source_user, target_user FROM "
    << "contactRequests WHERE target_user = '" << userId << "' AND accepted IS NULL", into(contactRequest.SourceUserId),
     into(contactRequest.TargetUserId));
    st3.execute();
    while (st3.fetch())
    {
        contactRequests.push_back(contactRequest);
    }
    UserState userState;
    userState.User = user;
    userState.ContactRequests = contactRequests;
    userState.Contacts = contacts;

    return std::optional<UserState>{userState};
}
std::optional<ContactRequestConfirmModel> Store::SetContactRequestConfirm(ContactRequestConfirmModel model){
    std::string accepted;
    if(model.Confirm){
        accepted = "Y";
    } else{
        accepted = "N";
    }
    std::cout << "Accepted:" << accepted << std::endl;
    int count;
    session.sql << "SELECT count(*) FROM users WHERE id = '" << model.SourceUserId << "' ", into(count);
    if(count == 0) throw std::runtime_error(("Użytkownik " + model.SourceUserId + " nie istnieje"));
    session.sql << "SELECT count(*) FROM users WHERE id = '" << model.TargetUserId << "' ", into(count);
    if(count == 0) throw std::runtime_error(("Użytkownik " + model.TargetUserId + " nie istnieje"));
    session.sql << "DELETE FROM contactRequests WHERE source_user = '"
    << model.SourceUserId << "' AND target_user = '" << model.TargetUserId << "'";
    session.sql << "DELETE FROM contactRequests WHERE source_user = '"
    << model.TargetUserId << "' AND target_user = '" << model.SourceUserId << "'";
    if(model.Confirm){
        session.sql << "INSERT INTO contacts(source_user, target_user) "
        << "VALUES(:source_user, :target_user)", use(model.SourceUserId), use(model.TargetUserId);
        session.sql << "INSERT INTO contacts(source_user, target_user) "
        << "VALUES(:target_user, :source_user)", use(model.TargetUserId), use(model.SourceUserId);
    }
    session.sql.commit();
    return std::optional<ContactRequestConfirmModel>{model};
}