#include<Server.hpp>

void Server::Run(){
    CreateSocket();
    BindAndListen();
    ResetMasks();
    fdmax = fd;
    std::cout << "Server listens on port " << provider.GetPort() << std::endl;
    while (true)
    {
        wmask = wmask_new;
        rmask = rmask_new;
        FD_SET(fd, &rmask);
        timeout.tv_sec = 5 * 60;
        timeout.tv_usec = 0;
        selectCount = select(fdmax+1, &rmask, &wmask, (fd_set*)0, &timeout);
        if(selectCount == 0){
            continue;
        }
        if (FD_ISSET(fd, &rmask)){
            AcceptClient();
        }
        for (int i = fd+1; i <= fdmax && selectCount > 0; i++){
            ReadFromClient(i);
        }
    }

    close(fd);
}

void Server::CreateSocket(){
    fd = socket(PF_INET, SOCK_STREAM, 0);
    on = 1;
    setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char *)&on, sizeof(on));
}

void Server::BindAndListen(){
    addr.sin_family = PF_INET;
    addr.sin_port = htons(provider.GetPort());
    addr.sin_addr.s_addr = INADDR_ANY;
    if (bind(fd, (struct sockaddr *)&addr, sizeof(addr)) != 0)
    {
        throw std::runtime_error(std::string(strerror(errno)));
    }
    if (listen(fd, 10) != 0)
    {
        throw std::runtime_error(std::string(strerror(errno)));
    };
}

void Server::AcceptClient(){
    selectCount -= 1;
    Client client;
    client.client_addr_length = sizeof(client.client_addr);
    client.fd = accept(fd, (struct sockaddr*)&client.client_addr, &client.client_addr_length);
    client.receiveBuf = std::vector<char>(provider.GetReceiveBufferSize());
    client.uuid = gen();
    clients[client.fd] = client;
    FD_SET(client.fd, &rmask_new);
    if (client.fd > fdmax) fdmax = client.fd;
}

void Server::ReadFromClient(int i){
    if(FD_ISSET(i, &rmask)){
        selectCount -= 1;
        std::vector<char> req;
        int received;
        auto buf = clients[i].receiveBuf;
        buf.resize(provider.GetReceiveBufferSize());
        if((received = 
                read(i, buf.data(), buf.size())
                )){
            std::cout << "received:" << received << std::endl;
            req.insert(std::end(req), buf.begin(), buf.begin() + received);
            std::cout << "request size:" << req.size() << std::endl;
        }
        if(!req.empty()){
            std::cout << "Request not empty, processing" << std::endl;
            auto length = (size_t)req[0];
            if(req.size() > length - 1){
                //header
                int headerBegin = 1;
                int headerEnd = headerBegin + length;
                auto messageType = std::string(req.begin() + headerBegin, req.begin() + headerEnd);
                std::cout << "MESSAGE_TYPE:" << messageType << std::endl;
                //bodyHeader
                int bodyHeaderBegin = headerEnd;
                int bodyHeaderEnd = bodyHeaderBegin + 3;
                size_t dataLength = size_t(
                    (unsigned char)*(req.begin() + bodyHeaderBegin) |
                    (unsigned char)*(req.begin() + bodyHeaderBegin + 1) << 8 |
                    (unsigned char)*(req.begin() + bodyHeaderBegin + 2) << 16 |
                    (unsigned char)*(req.begin() + bodyHeaderBegin + 3) << 24
                     );
                int idBegin = bodyHeaderEnd + 1;
                int idEnd = idBegin + 36;
                auto correlationId = std::string(req.begin() + idBegin, req.begin() + idEnd);
                std::cout << "CORRELATION_ID:" << correlationId << std::endl;
                int tokenLengthBegin = idEnd;
                int tokenLengthEnd = idEnd + 1;
                size_t tokenLength = size_t(
                    (unsigned char)*(req.begin() + tokenLengthBegin) |
                    (unsigned char)*(req.begin() + tokenLengthBegin + 1) << 8
                );
                std::cout << "TOKEN_LENGTH:" << tokenLength << std::endl;
                int tokenBegin = tokenLengthEnd + 1;
                int tokenEnd = tokenBegin + tokenLength;
                auto token = std::string(req.begin() + tokenBegin, req.begin() + tokenEnd);
                std::cout << "TOKEN:" << token << std::endl;
                int bodyBegin = tokenEnd;
                int distance = std::distance(req.begin(), req.begin() + bodyBegin);
                ssize_t remainingData = distance + dataLength - received;
                std::cout << "distance:" << distance << std::endl;
                std::cout << "remainingData:" << remainingData << std::endl;
                std::cout << "dataLength:" << dataLength << std::endl;
                while(dataLength > req.size()){
                    received = read(i, buf.data(), buf.size());
                    std::cout << "received:" << received << std::endl;

                    if(remainingData < received){
                        //trim excess data, bigger than dataLength
                        std::cout << "trimming response" << std::endl;
                        received = remainingData;
                    }
                    std::cout << "reading:" << std::distance(buf.begin(), buf.begin() + received) << std::endl;
                    req.insert(std::end(req), buf.begin(), buf.begin() + received);
                    remainingData -= received;
                    std::cout << "remaining data:" << remainingData << std::endl;
                    std::cout << "request size:" << req.size() << std::endl;
                }

                //body
                int bodyEnd = bodyBegin + dataLength;
                std::cout << "final request distance:" << std::distance(req.begin(), req.begin() + bodyEnd) << std::endl;
                std::cout << "final request size:" << req.size() << std::endl;
                auto message = std::string(req.begin() + bodyBegin,req.begin() + bodyEnd);
                std::cout << messageType << " received" << std::endl;
                std::cout << message << std::endl;
                receivedSignal(i, clients[i].uuid, messageType, correlationId, token, message);
            } else{
                std::cout << "Request too small, closing connection" << std::endl;
                clients.erase(i);
                close(i);
                FD_CLR(i, &rmask_new);
                if(i == fdmax) fdmax--;
            }
        } else{
            std::cout << "Closing connection with client" << std::endl;
            clients.erase(i);
            close(i);
            FD_CLR(i, &rmask_new);
            if(i == fdmax) fdmax--;
            
        }
    }
}

void Server::ResetMasks(){
    FD_ZERO(&rmask_new);
    FD_ZERO(&wmask_new);
    FD_ZERO(&rmask);
    FD_ZERO(&wmask);
}

void Server::Send(int fd, boost::uuids::uuid uuid, std::string message){
    const auto client = clients[fd]; 
    std::cout << "Preparing to send" << std::endl;
    std::cout << "Request fd:" << fd << std::endl;
    std::cout << "uuid:" << to_string(uuid) << std::endl;
    std::cout << "Client fd:" << client.fd << std::endl;
    std::cout << "Client uuid:" << to_string(client.uuid) << std::endl;
    if(client.uuid == uuid){
        std::cout << "Matched uuid, sending to client" << std::endl;
        write(fd, message.data(), message.size());
    }
}