#include <Services/UserService.hpp>

std::optional<std::string> UserService::Login(std::string login, std::string password){
    auto user = store.GetUser(login);
    if(user.has_value()){
        std::cout << "Salt: " << user->Salt << std::endl;
        auto hashedPassword = HashPassword(password + user->Salt);
        if(hashedPassword == user->HashedPassword){
            auto token = GenerateSalt();
            store.AddToken(login, token);
            std::cout << "Token: " << token << std::endl; 
            return std::optional<std::string>{token};
        }
    }
    return std::nullopt;
}

bool UserService::Register(std::string login, std::string password){
    User user;
    user.Id = login;
    user.Salt = GenerateSalt();
    auto hashedPassword = HashPassword(password + user.Salt);
    user.HashedPassword = hashedPassword;
    return store.AddUser(user);
}

std::optional<std::vector<Contact>> UserService::GetContacts(ContactsQuery query){
    return store.GetContacts(query);
}

std::optional<ContactRequestModel> UserService::AddContactRequest(ContactRequestModel request){
    return store.AddContactRequest(request);
}
std::optional<SendMessageModel> UserService::SendMessage(SendMessageModel message){
    return store.SendMessage(message);
}
std::optional<std::vector<std::string>> UserService::GetCountries(){
    return store.GetCountries();
}
std::optional<std::vector<SettingModel>> UserService::GetSettings(std::string userId){
    return store.GetSettings(userId);
}
std::optional<std::vector<SettingModel>> UserService::SetSettings(std::vector<SettingModel> settings, std::string userId){
    return store.SetSettings(settings, userId);
}
std::optional<UserModel> UserService::SetUserDataHandler(UserModel model){
    return store.SetUserDataHandler(model);
}
std::optional<UserState> UserService::GetUserState(std::string userId){
    return store.GetUserState(userId);
}
std::optional<ContactRequestConfirmModel> UserService::SetContactRequestConfirm(ContactRequestConfirmModel model){
    return store.SetContactRequestConfirm(model);
}