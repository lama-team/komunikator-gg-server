
https://github.com/toeb/moderncmake

https://crascit.com/2015/07/25/cmake-gtest/

https://cmake.org/cmake/help/v3.10/module/GoogleTest.html#command:gtest_discover_tests

To build container and start oracle db:

Run once:
docker network create final

Then:
docker run -d -p 8080:8080 -p 1521:1521 --network final --name database sath89/oracle-xe-11g
or
docker start database

Then to start container:
GRPC:
docker run -it -p 1025:1025 --network final -e ORACLE_URI=database:1521 server