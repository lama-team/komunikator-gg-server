FROM adiq94/base-komunikator
RUN mkdir -p /app
WORKDIR /app
COPY . .
RUN cmake .
RUN make -j4 server
RUN chmod +x ./server


EXPOSE 1024
CMD ["/app/server"]