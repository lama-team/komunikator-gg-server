#ifndef CONST_H
#define CONST_H
#include <string>
namespace Constants{
    extern const std::string& EchoRequest;
    extern const std::string& EchoSuccessResponse;
    extern const std::string& EchoErrorResponse;

    extern const std::string& LoginRequest;
    extern const std::string& LoginSuccessResponse;
    extern const std::string& LoginErrorResponse;

    extern const std::string& RegisterRequest;
    extern const std::string& RegisterSuccessResponse;
    extern const std::string& RegisterErrorResponse;

    extern const std::string& GetContactsRequest;
    extern const std::string& GetContactsSuccessResponse;
    extern const std::string& GetContactsErrorResponse;

    extern const std::string& MessageNotificationPush;
    extern const std::string& MessageNotificationConfirm;

    extern const std::string& ContactRequestRequest;
    extern const std::string& ContactRequestSuccessResponse;
    extern const std::string& ContactRequestErrorResponse;

    extern const std::string& ContactRequestNotificationPush;
    extern const std::string& ContactRequestNotificationConfirm;

    extern const std::string& SetSettingsRequest;
    extern const std::string& SetSettingsErrorResponse;
    extern const std::string& SetSettingsSuccessResponse;

    extern const std::string& GetSettingsRequest;
    extern const std::string& GetSettingsSuccessResponse;
    extern const std::string& GetSettingsErrorResponse;

    extern const std::string& SendMessageRequest;
    extern const std::string& SendMessageSuccessResponse;
    extern const std::string& SendMessageErrorResponse;

    extern const std::string& SetUserDataRequest;
    extern const std::string& SetUserDataSuccessResponse;
    extern const std::string& SetUserDataErrorResponse;

    extern const std::string& GetCountriesRequest;
    extern const std::string& GetCountriesSuccessResponse;
    extern const std::string& GetCountriesErrorResponse;

    extern const std::string& GetUserStateRequest;
    extern const std::string& GetUserStateSuccessResponse;
    extern const std::string& GetUserStateErrorResponse;

    extern const std::string& ContactRequestNotificationConfirm;
    extern const std::string& ContactRequestNotificationConfirmSuccessResponse;
    extern const std::string& ContactRequestNotificationConfirmErrorResponse;
}
#endif //CONST_H