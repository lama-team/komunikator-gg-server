#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include <messages.pb.h>

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <ctime>
#include <google/protobuf/util/time_util.h>

using google::protobuf::util::TimeUtil;

// Simple test, does not use gmock
TEST(EchoRequest, CanCreate)
{
    komunikator::EchoRequest request;
    boost::uuids::random_generator gen;
    boost::uuids::uuid u = gen();
    std::string uuid = to_string(u);
    request.set_correlationid(uuid);
    request.set_message("message");
    *request.mutable_created() = TimeUtil::SecondsToTimestamp(time(NULL));
    //request = TimeUtil::SecondsToTimestamp(time(NULL));
    EXPECT_EQ(request.correlationid(), uuid);
}

TEST(Dummy, foobara)
{
    EXPECT_EQ(1, 1);
}

// Real class we want to mock
class TeaBreak
{
public:
    virtual ~TeaBreak() {}

    // Return minutes taken to make the drinks
    int morningTea()
    {
        return makeCoffee(true,  1) +
               makeCoffee(false, 0.5) +
               makeHerbalTea();
    }

private:
    virtual int makeCoffee(bool milk, double sugars) = 0;
    virtual int makeHerbalTea() = 0;
};

// Mock class
class MockTeaBreak : public TeaBreak
{
public:
    MOCK_METHOD2(makeCoffee,    int(bool milk, double sugars));
    MOCK_METHOD0(makeHerbalTea, int());
};


using ::testing::Return;
using ::testing::_;

// Mocked test
TEST(TeaBreakTest, MorningTea)
{
    MockTeaBreak  teaBreak;
    EXPECT_CALL(teaBreak, makeCoffee(_,_))
        .WillOnce(Return(2))
        .WillOnce(Return(1));
    EXPECT_CALL(teaBreak, makeHerbalTea())
        .WillOnce(Return(3));

    EXPECT_LE(teaBreak.morningTea(), 6);
}