#include <Constants/const.hpp>
namespace Constants{
    const std::string& EchoRequest = "EchoRequest";
    const std::string& EchoSuccessResponse = "EchoSuccessResponse";
    const std::string& EchoErrorResponse = "EchoErrorResponse";

    const std::string& LoginRequest = "LoginRequest";
    const std::string& LoginSuccessResponse = "LoginSuccessResponse";
    const std::string& LoginErrorResponse = "LoginErrorResponse";

    const std::string& RegisterRequest = "RegisterRequest";
    const std::string& RegisterSuccessResponse = "RegisterSuccessResponse";
    const std::string& RegisterErrorResponse = "RegisterErrorResponse";

    const std::string& GetContactsRequest = "GetContactsRequest";
    const std::string& GetContactsSuccessResponse = "GetContactsSuccessResponse";
    const std::string& GetContactsErrorResponse = "GetContactsErrorResponse";

    const std::string& MessageNotificationPush = "MessageNotificationPush";
    const std::string& MessageNotificationConfirm = "MessageNotificationConfirm";

    const std::string& ContactRequestRequest = "ContactRequestRequest";
    const std::string& ContactRequestSuccessResponse = "ContactRequestSuccessResponse";
    const std::string& ContactRequestErrorResponse = "ContactRequestErrorResponse";

    const std::string& ContactRequestNotificationPush = "ContactRequestNotificationPush";

    const std::string& SetSettingsRequest = "SetSettingsRequest";
    const std::string& SetSettingsErrorResponse = "SetSettingsErrorResponse";
    const std::string& SetSettingsSuccessResponse = "SetSettingsSuccessResponse";

    const std::string& GetSettingsRequest = "GetSettingsRequest";
    const std::string& GetSettingsSuccessResponse = "GetSettingsSuccessResponse";
    const std::string& GetSettingsErrorResponse = "GetSettingsErrorResponse";

    const std::string& SendMessageRequest = "SendMessageRequest";
    const std::string& SendMessageSuccessResponse = "SendMessageSuccessResponse";
    const std::string& SendMessageErrorResponse = "SendMessageErrorResponse";

    const std::string& SetUserDataRequest = "SetUserDataRequest";
    const std::string& SetUserDataSuccessResponse = "SetUserDataSuccessResponse";
    const std::string& SetUserDataErrorResponse = "SetUserDataErrorsResponse";

    const std::string& GetCountriesRequest = "GetCountriesRequest";
    const std::string& GetCountriesSuccessResponse = "GetCountriesSuccessResponse";
    const std::string& GetCountriesErrorResponse = "GetCountriesErrorResponse";

    const std::string& GetUserStateRequest = "GetUserStateRequest";
    const std::string& GetUserStateSuccessResponse = "GetUserStateSuccessResponse";
    const std::string& GetUserStateErrorResponse = "GetUserStateErrorResponse";

    const std::string& ContactRequestNotificationConfirm = "ContactRequestNotificationConfirm";
    const std::string& ContactRequestNotificationConfirmSuccessResponse = "ContactRequestNotificationConfirmSuccessResponse";
    const std::string& ContactRequestNotificationConfirmErrorResponse = "ContactRequestNotificationConfirmErrorResponse";
}