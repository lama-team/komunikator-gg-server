#include <iostream>
#include <boost/di.hpp>
// #include <string>
// #include <map>
// #include <stdexcept>
//#include <Server.hpp>
// #include <Client/ClientGateway.hpp>
// #include <Client/Dispatcher.hpp>
// #include <Model/Handler.hpp>
// #include <Handlers/ContactRequestHandler.hpp>
// #include <Handlers/EchoHandler.hpp>
// #include <Handlers/GetContactsHandler.hpp>
// #include <Handlers/GetCountriesHandler.hpp>
// #include <Handlers/GetSettingsHandler.hpp>
// #include <Handlers/LoginHandler.hpp>
// #include <Handlers/RegisterHandler.hpp>
// #include <Handlers/SendMessageRequestHandler.hpp>
// #include <Handlers/SetSettingsHandler.hpp>
// #include <Handlers/SetUserDataHandler.hpp>
// #include <Handlers/ContactRequestNotificationConfirm.hpp>
// #include <Handlers/GetUserStateHandler.hpp>
#include <Database/Migration.hpp>
#include <Handlers/GRPCHandler.hpp>


namespace di = boost::di;
 
int main()
{
    const auto injector = di::make_injector();
    // Server &server = injector.create<Server&>();
    // Dispatcher &dispatcher = injector.create<Dispatcher&>();
    // server.receivedSignal.connect(boost::bind(&Dispatcher::Receive, dispatcher, _1, _2, _3, _4, _5, _6));
    Migration migration = injector.create<Migration>();
    migration.Execute(true, true);
    // server.Run();
    std::string server_address("0.0.0.0:1025");
    komunikator::UserServiceImpl &service = injector.create<komunikator::UserServiceImpl&>();
    ::grpc::Service *ptr = &service;
    ServerBuilder builder;
    builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
    builder.RegisterService(ptr);
    std::unique_ptr<Server> server(builder.BuildAndStart());
    std::cout << "Server listening on " << server_address << std::endl;
    server->Wait();
    return 0;
}