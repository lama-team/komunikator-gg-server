FROM base/archlinux
RUN cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup
RUN sed -i 's/^#Server/Server/' /etc/pacman.d/mirrorlist.backup
RUN rankmirrors -n 6 /etc/pacman.d/mirrorlist.backup > /etc/pacman.d/mirrorlist
RUN pacman -Syu --noconfirm
RUN pacman -S git cmake boost protobuf gmock crypto++ gcc make wget sudo perl vim unzip rpmextract libaio --noconfirm

WORKDIR /tmp
RUN useradd -m -G wheel -s /bin/bash archie
RUN echo "%wheel ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
USER archie
RUN sudo -S true

USER root
COPY oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm .
COPY oracle-instantclient12.2-devel-12.2.0.1.0-1.x86_64.rpm .
RUN rpmextract.sh oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm
RUN rpmextract.sh oracle-instantclient12.2-devel-12.2.0.1.0-1.x86_64.rpm
RUN cp -Rv usr/* /usr
RUN git clone https://github.com/SOCI/soci.git
WORKDIR /tmp/soci
RUN cmake -G "Unix Makefiles" -DWITH_BOOST=ON -DWITH_ORACLE=ON  .
RUN make
RUN make install
WORKDIR /tmp
RUN git clone https://github.com/grpc/grpc.git
WORKDIR /tmp/grpc
RUN git checkout v1.8.3
RUN git submodule update --init
# Install c-ares
WORKDIR /tmp/grpc/third_party/cares/cares
RUN git fetch origin
RUN git checkout cares-1_13_0
RUN cmake -DCMAKE_BUILD_TYPE=Release .
RUN make -j4 install
WORKDIR /tmp/grpc/
RUN rm -rf third_party/cares/cares
#
RUN cmake -DgRPC_INSTALL=ON -DgRPC_BUILD_TESTS=OFF -DgRPC_PROTOBUF_PROVIDER=package -DgRPC_ZLIB_PROVIDER=package -DgRPC_CARES_PROVIDER=package -DgRPC_SSL_PROVIDER=package -DCMAKE_BUILD_TYPE=Release .
RUN make -j4 install

# Add Tini
ENV TINI_VERSION v0.16.1
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini
ENTRYPOINT ["/tini", "--"]